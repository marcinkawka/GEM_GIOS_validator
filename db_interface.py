from configparser import ConfigParser
from psycopg2 import sql
from psycopg2.extensions import AsIs
import psycopg2
import pandas as pd
'''
Contents:

0. Test, Conect, etc.
1. delete
2. get
3. store
4. update
5. wrappers fo PGfunctions
'''
class DBConnection():
	'''
	Interface for interacting with wios data, stored in PostgreSQL db
	'''
	def __init__(self,file='/home/mkawka/pythony/dash_trials/config.ini'):
		self.file=file
		self.parser = ConfigParser()
		self.parser.read(file)
		
	def __del__(self):
		try:
			self.conn.close()	
		except:
			print('I was unable to close connection - never connected?')	

	def connect(self, section='postgresql'):
		db = {}
		if self.parser.has_section(section):
			params = self.parser.items(section)
			for param in params:
				db[param[0]] = param[1]
		else:
			raise Exception('Section {0} not found in the {1} file'.format(section, self.file))
		
		try:
			conn= psycopg2.connect("dbname="+db['db_name']+" user="+db['user']+ " host="+db['host']+" password="+db['password'])
		except Exception as e:
			print('I am unable to connect to '+db['host'])
			print(e)
			
		self.conn=conn

	def test(self):
		sql="SELECT datname from pg_database"
		cur= self.conn.cursor()
		cur.execute(sql)
		rows = cur.fetchall()
		print("\nShow me the databases:\n")
		for row in rows:
			print("   ", row[0])

	''' *****************************************
	DELETE methods
	*********************************************'''
	def deleteNullResults(self):
		sql="DELETE from operational.results where value='NaN'"
		try:
			cur= self.conn.cursor()
			cur.execute(sql)
		except:
			print('Error deleting NaN values')	

	''' ******************************************
	GET methods 
	**********************************************'''

	def getStations(self):
		'''Lists stations from the database'''
		sql="Select id_station,station_name,station_address,station_lat,station_long,station_code from operational.stations"
		cur= self.conn.cursor()
		cur.execute(sql)
		rows = cur.fetchall()
		return rows
		
	def getGEMStations(self):
		'''Lists stations from extracting GEM_Results'''
		sql="Select id_station,station_code, gem_lat,gem_long, gem_idx_lat,gem_idx_lon from operational.validation_stations"
		cur= self.conn.cursor()
		cur.execute(sql)
		rows = cur.fetchall()
		return rows

	def getDevices(self):
		'''populates devices list from db'''
		sql="select id_measurement from operational.measurements"
		cur=self.conn.cursor()
		cur.execute(sql)
		rows=cur.fetchall()	
		return rows

	def getNumberOfStations(self,date2=None):
		'''gets number of each type stations used to calculate aggregates on a given date
		if no date is given - the latest avaliable day'''
		if date2 is None:
			sql="select count(v.station_name) as liczba_stacji, \
			v.paramname,v.unixdate,v.station_type,v.area_type \
			from public.v_operational_monitoring v \
			where v.unixdate=(select max(v2.unixdate) from public.v_operational_monitoring v2) \
			group by v.station_type,v.area_type,unixdate,v.paramname \
			order by unixdate,paramname desc"
			
			cur=self.conn.cursor()	
			try:
				cur.execute(sql)
				df=pd.DataFrame(cur.fetchall())
				df.columns=['liczba_stacji','paramname','date','station_type','area_type']			
			except Exception as e:
				print('I am unable to fetch number of stations ')
				print(e)
			return df	
		else:				
			sqlQ="select count(v.station_name) as liczba_stacji, \
			v.paramname,v.unixdate,v.station_type,v.area_type \
			from public.v_operational_monitoring v \
			where v.unixdate=extract( epoch from timestamp with time zone %s ) \
			group by v.station_type,v.area_type,unixdate,v.paramname \
			order by unixdate,paramname desc"
			
			cur=self.conn.cursor()	
			try:
				cur.execute(sqlQ,(date2.strftime('%Y-%m-%d 00:00:00+01'),))
				df=pd.DataFrame(cur.fetchall())
				df.columns=['liczba_stacji','paramname','date','station_type','area_type']			
			except Exception as e:
				print('I am unable to fetch number of stations ')
				print(e)
			return df					
		

	def getPdLatestErrors(self,param,type):
		'''populates latest errors as pandas Dataframe'''
		if type == 'all':
			
			sqlQ="select err.id_station,mae,me,rmsd,nrmsd,cov,cor,station_long,station_lat,station_name,to_timestamp(err.unixdate) \
				from operational.error_stats_all err \
				join operational.stations st on st.id_station=err.id_station \
				where paramname=%s and unixdate=(select max(unixdate) from operational.error_stats_all)"
			cur=self.conn.cursor()	

			try:
				cur.execute(sqlQ,(param,))
				df=pd.DataFrame(cur.fetchall())
				df.columns=['id_station','mae','me','rmsd','nrmsd','cov','cor','station_long','station_lat','station_name','date']
			except Exception as e:
				print('I am unable to fetch errors of '+str(param)+'  for '+str(type)+' from db')
				print(e)
			return df	

		elif type == 'urban':
			sqlQ="select err.id_station,mae,me,rmsd,nrmsd,cov,cor,station_long,station_lat,station_name,to_timestamp(err.unixdate) \
				from operational.error_stats_bcg_urban err \
				join operational.stations st on st.id_station=err.id_station \
				where paramname=%s and unixdate=(select max(unixdate) from operational.error_stats_bcg_urban)"
			
			cur=self.conn.cursor()	

			try:
				cur.execute(sqlQ,(param,))
				df=pd.DataFrame(cur.fetchall())
				df.columns=['id_station','mae','me','rmsd','nrmsd','cov','cor','station_long','station_lat','station_name','date']
			except Exception as e:
				print('I am unable to fetch errors of '+str(param)+'  for '+str(type)+' from db')
				print(e)
			return df

		elif type == 'suburban':
			sqlQ="select err.id_station,mae,me,rmsd,nrmsd,cov,cor,station_long,station_lat,station_name,to_timestamp(err.unixdate) \
				from operational.error_stats_bcg_suburban err \
				join operational.stations st on st.id_station=err.id_station \
				where paramname=%s and unixdate=(select max(unixdate) from operational.error_stats_bcg_suburban)"
			
			cur=self.conn.cursor()	

			try:
				cur.execute(sqlQ,(param,))
				df=pd.DataFrame(cur.fetchall())
				df.columns=['id_station','mae','me','rmsd','nrmsd','cov','cor','station_long','station_lat','station_name','date']
			except Exception as e:
				print('I am unable to fetch errors of '+str(param)+'  for '+str(type)+' from db')
				print(e)
			return df

		elif type == 'rural':
			sqlQ="select err.id_station,mae,me,rmsd,nrmsd,cov,cor,station_long,station_lat,station_name,to_timestamp(err.unixdate) \
				from operational.error_stats_bcg_rural err \
				join operational.stations st on st.id_station=err.id_station \
				where paramname=%s and unixdate=(select max(unixdate) from operational.error_stats_bcg_rural)"
			
			cur=self.conn.cursor()	

			try:
				cur.execute(sqlQ,(param,))
				df=pd.DataFrame(cur.fetchall())
				df.columns=['id_station','mae','me','rmsd','nrmsd','cov','cor','station_long','station_lat','station_name','date']
			except Exception as e:
				print('I am unable to fetch errors of '+str(param)+'  for '+str(type)+' from db')
				print(e)
			return df

		elif type == 'traffic':
			sqlQ="select err.id_station,mae,me,rmsd,nrmsd,cov,cor,station_long,station_lat,station_name,to_timestamp(err.unixdate) \
				from operational.error_stats_traffic err \
				join operational.stations st on st.id_station=err.id_station \
				where paramname=%s and unixdate=(select max(unixdate) from operational.error_stats_traffic)"
			
			cur=self.conn.cursor()	

			try:
				cur.execute(sqlQ,(param,))
				df=pd.DataFrame(cur.fetchall())
				df.columns=['id_station','mae','me','rmsd','nrmsd','cov','cor','station_long','station_lat','station_name','date']
			except Exception as e:
				print('I am unable to fetch errors of '+str(param)+'  for '+str(type)+' from db')
				print(e)
			return df

		elif type == 'industrial':	
			sqlQ="select err.id_station,mae,me,rmsd,nrmsd,cov,cor,station_long,station_lat,station_name,to_timestamp(err.unixdate) \
				from operational.error_stats_industrial err \
				join operational.stations st on st.id_station=err.id_station \
				where paramname=%s and unixdate=(select max(unixdate) from operational.error_stats_industrial)"
			
			cur=self.conn.cursor()	

			try:
				cur.execute(sqlQ,(param,))
				df=pd.DataFrame(cur.fetchall())
				df.columns=['id_station','mae','me','rmsd','nrmsd','cov','cor','station_long','station_lat','station_name','date']
			except Exception as e:
				print('I am unable to fetch errors of '+str(param)+'  for '+str(type)+' from db')
				print(e)
			return df

	def getPdAggregates(self,param,type,t0,tk):
		'''populates aggregates as pandas Dataframe'''
		if type == 'all':
			sqlQ="SELECT unixdate,gios_avg,gem_avg,gios_median,gem_median,gios_stddev,gem_stddev, \
			gios_p10,gem_p10,gios_p25,gem_p25,gios_p75,gem_p75,gios_p90,gem_p90, \
			gios_min,gem_min,gios_max,gem_max \
			from v_aggregates \
			WHERE unixdate  >= %s and unixdate<= %s and paramname=%s"
			cur=self.conn.cursor()
			try:
				cur.execute(sql.SQL(sqlQ),(str(t0),str(tk),str(param)))
				
				df=pd.DataFrame(cur.fetchall())
				df.columns=['unixdate','gios_avg','gem_avg','gios_median','gem_median','gios_stddev','gem_stddev',\
				'gios_p10','gem_p10','gios_p25','gem_p25','gios_p75','gem_p75','gios_p90','gem_p90',\
				'gios_min','gem_min','gios_max','gem_max']

			except Exception as e:
				print('I am unable to fetch aggregates '+str(param)+'  for '+str(type)+' from db')
				print(e)

			return df
		elif type =='urban':
			sqlQ="SELECT unixdate,gios_avg,gem_avg,gios_median,gem_median,gios_stddev,gem_stddev, \
			gios_p10,gem_p10,gios_p25,gem_p25,gios_p75,gem_p75,gios_p90,gem_p90, \
			gios_min,gem_min,gios_max,gem_max \
			from v_aggregates_urban \
			WHERE unixdate  >= %s and unixdate<= %s and paramname=%s"
			cur=self.conn.cursor()
			try:
				cur.execute(sql.SQL(sqlQ),(str(t0),str(tk),str(param)))
				
				df=pd.DataFrame(cur.fetchall())
				df.columns=['unixdate','gios_avg','gem_avg','gios_median','gem_median','gios_stddev','gem_stddev',\
				'gios_p10','gem_p10','gios_p25','gem_p25','gios_p75','gem_p75','gios_p90','gem_p90',\
				'gios_min','gem_min','gios_max','gem_max']

			except Exception as e:
				print('I am unable to fetch aggregates '+str(param)+'  for '+str(type)+' from db')
				print(e)

		elif type =='suburban':
			sqlQ="SELECT unixdate,gios_avg,gem_avg,gios_median,gem_median,gios_stddev,gem_stddev, \
			gios_p10,gem_p10,gios_p25,gem_p25,gios_p75,gem_p75,gios_p90,gem_p90, \
			gios_min,gem_min,gios_max,gem_max \
			from v_aggregates_suburban \
			WHERE unixdate  >= %s and unixdate<= %s and paramname=%s"
			cur=self.conn.cursor()
			try:
				cur.execute(sql.SQL(sqlQ),(str(t0),str(tk),str(param)))
				
				df=pd.DataFrame(cur.fetchall())
				df.columns=['unixdate','gios_avg','gem_avg','gios_median','gem_median','gios_stddev','gem_stddev',\
				'gios_p10','gem_p10','gios_p25','gem_p25','gios_p75','gem_p75','gios_p90','gem_p90',\
				'gios_min','gem_min','gios_max','gem_max']

			except Exception as e:
				print('I am unable to fetch aggregates '+str(param)+'  for '+str(type)+' from db')
				print(e)

		elif type =='rural':
			sqlQ="SELECT unixdate,gios_avg,gem_avg,gios_median,gem_median,gios_stddev,gem_stddev, \
			gios_p10,gem_p10,gios_p25,gem_p25,gios_p75,gem_p75,gios_p90,gem_p90, \
			gios_min,gem_min,gios_max,gem_max \
			from v_aggregates_rural \
			WHERE unixdate  >= %s and unixdate<= %s and paramname=%s"
			cur=self.conn.cursor()
			try:
				cur.execute(sql.SQL(sqlQ),(str(t0),str(tk),str(param)))
				
				df=pd.DataFrame(cur.fetchall())
				df.columns=['unixdate','gios_avg','gem_avg','gios_median','gem_median','gios_stddev','gem_stddev',\
				'gios_p10','gem_p10','gios_p25','gem_p25','gios_p75','gem_p75','gios_p90','gem_p90',\
				'gios_min','gem_min','gios_max','gem_max']

			except Exception as e:
				print('I am unable to fetch aggregates '+str(param)+'  for '+str(type)+' from db')
				print(e)

		elif type =='industrial':
			sqlQ="SELECT unixdate,gios_avg,gem_avg,gios_median,gem_median,gios_stddev,gem_stddev, \
			gios_p10,gem_p10,gios_p25,gem_p25,gios_p75,gem_p75,gios_p90,gem_p90, \
			gios_min,gem_min,gios_max,gem_max \
			from v_aggregates_industrial \
			WHERE unixdate  >= %s and unixdate<= %s and paramname=%s"
			cur=self.conn.cursor()
			try:
				cur.execute(sql.SQL(sqlQ),(str(t0),str(tk),str(param)))
				
				df=pd.DataFrame(cur.fetchall())
				df.columns=['unixdate','gios_avg','gem_avg','gios_median','gem_median','gios_stddev','gem_stddev',\
				'gios_p10','gem_p10','gios_p25','gem_p25','gios_p75','gem_p75','gios_p90','gem_p90',\
				'gios_min','gem_min','gios_max','gem_max']

			except Exception as e:
				print('I am unable to fetch aggregates '+str(param)+'  for '+str(type)+' from db')
				print(e)

		elif type =='traffic':
			sqlQ="SELECT unixdate,gios_avg,gem_avg,gios_median,gem_median,gios_stddev,gem_stddev, \
			gios_p10,gem_p10,gios_p25,gem_p25,gios_p75,gem_p75,gios_p90,gem_p90, \
			gios_min,gem_min,gios_max,gem_max \
			from v_aggregates_traffic \
			WHERE unixdate  >= %s and unixdate<= %s and paramname=%s"
			cur=self.conn.cursor()
			try:
				cur.execute(sql.SQL(sqlQ),(str(t0),str(tk),str(param)))
				
				df=pd.DataFrame(cur.fetchall())
				df.columns=['unixdate','gios_avg','gem_avg','gios_median','gem_median','gios_stddev','gem_stddev',\
				'gios_p10','gem_p10','gios_p25','gem_p25','gios_p75','gem_p75','gios_p90','gem_p90',\
				'gios_min','gem_min','gios_max','gem_max']

			except Exception as e:
				print('I am unable to fetch aggregates '+str(param)+'  for '+str(type)+' from db')
				print(e)

		return df
	''' *********************************************
		STORE methods
	************************************************* '''

	def storeStations(self,stations_list):
		'''Stores stations in db'''
		sqlQ="INSERT INTO  operational.stations (id_station,station_name,station_address,station_lat,station_long)\
			SELECT %s,%s,%s,%s,%s \
				WHERE \
					NOT EXISTS(\
						SELECT id_station from operational.stations \
						WHERE id_station= %s \
					);"

		with self.conn.cursor() as cur:
			for st in stations_list:
				try:
					cur.execute(sql.SQL(sqlQ),(str(st[2]),str(st[0]),str(st[1]),str(st[3]),str(st[4]),str(st[2])))
					self.conn.commit()

				except Exception as err:
					print("Error adding "+str(st[0]))
					print(err)



	def storeGEMts1h(self,id_station,paramname,t0,ts):
		'''Stores timeseries, starting from t0 wih 1h timestep
		Achtung!
		Database stores GMT timestamp
		GEM-AQ runs in CET timzone
		'''

		sqlQ="INSERT INTO operational.gem_results(id_station,paramname,value,unixdate) \
			SELECT %s,%s,%s, EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s %s:00:00.00+01')"

		i=0
		for ti in ts: #iteration over timeseries items
			with self.conn.cursor() as cur:
				try:
					cur.execute(sql.SQL(sqlQ),(str(id_station),str(paramname),str(ti),AsIs(str(t0)),AsIs(str(i).zfill(2))))
					self.conn.commit()

				except Exception as err:
					print("Error adding "+str(id_station))
					print(err)					
					self.conn.rollback()
			i+=1


		print("Added "+str(paramname)+" at "+str(id_station))	



	def storeValidationStation(self,st):
		'''Stores GEM stations in db'''
		sqlQ='INSERT INTO  operational.validation_stations (id_station,station_code, gem_lat,gem_long, gem_idx_lat,gem_idx_lon)\
			SELECT %s,%s, %s,%s, %s,%s \
				WHERE \
					NOT EXISTS(\
						SELECT id_station from operational.validation_stations \
						WHERE id_station= %s \
					);'

		with self.conn.cursor() as cur:
			try:
				cur.execute(sql.SQL(sqlQ),(str(st[0]),str(st[1]), str(st[2]),str(st[3]), str(st[4]),str(st[5]), str(st[0])))
				self.conn.commit()
				print("Added "+st[1])	
			except Exception as err:
				print("Error adding "+str(st[0]))
				print(cur.mogrify(sql.SQL(sqlQ),(str(st[0]),str(st[1]), str(st[2]),str(st[3]), str(st[4]),str(st[5]), str(st[0]))))
				print(err)
				self.conn.rollback()

	def storeMeasurements(self,measurement_list):
		'''Stores measurements in db'''
		sqlQ="INSERT INTO  operational.measurements (id_measurement,id_station,paramname) \
			SELECT %s,%s,%s \
				WHERE \
					NOT EXISTS(\
						SELECT id_measurement from operational.measurements \
						WHERE id_measurement = %s \
					);"

		with self.conn.cursor() as cur:
			for st in measurement_list:
				try:
					cur.execute(sql.SQL(sqlQ),(str(st[0]),str(st[1]),str(st[2]),str(st[0])))
					self.conn.commit()

				except Exception as err:
					print("Error adding "+str(st[0]))
					print(str(err))
					self.conn.rollback()
	
	def storeResults(self,results_list):
		'''Stores measurements in db'''
		sqlQ="INSERT INTO  operational.results (id_measurement,value,unix_date) \
			SELECT %s,%s,%s  \
			WHERE \
			 	NOT EXISTS(\
			 		SELECT id_measurement,unix_date from operational.results\
			 		WHERE id_measurement= %s and unix_date =%s \
			 	);"
		
		with self.conn.cursor() as cur:
			for st in results_list:
				try:
					cur.execute(sql.SQL(sqlQ),(str(st[2]),str(st[1]),str(st[0]),str(st[2]),str(st[0])))
					self.conn.commit()

				except Exception as err:
					print("Error adding "+str(st))
					print(str(err))
					self.conn.rollback()

	''' ***************************************
		UPDATE methods
	******************************************* '''

	def updateStationCode(self,staion_id,station_code):
		'''updates station code for given station id'''
		sqlQ="UPDATE operational.stations set station_code = %s where id_station = %s"
		with self.conn.cursor() as cur:
			try:
				cur.execute(sql.SQL(sqlQ),(str(station_code),str(staion_id)))
				self.conn.commit()
			except Exception as err:
				print("Error adding "+str(station_code))
				print(err)
				self.conn.rollback()

	def updateStationType(self,staion_id,station_type):
		'''updates station type for given station id'''
		sqlQ="UPDATE operational.stations set station_type = %s where id_station = %s"
		with self.conn.cursor() as cur:
			try:
				cur.execute(sql.SQL(sqlQ),(str(station_type),str(staion_id)))
				self.conn.commit()
			except Exception as err:
				print("Error adding "+str(station_type))
				print(err)
				self.conn.rollback()

	def updateAreaType(self,staion_id,area_type):
		'''updates area type for given station id'''
		sqlQ="UPDATE operational.stations set area_type = %s where id_station = %s"
		with self.conn.cursor() as cur:
			try:
				cur.execute(sql.SQL(sqlQ),(str(area_type),str(staion_id)))
				self.conn.commit()

			except Exception as err:
				print("Error adding "+str(area_type))
				print(err)
				self.conn.rollback()

	''' ***************************************
		WRAPPERS for PgSQL functions
	******************************************* '''				
	def calculate_stats_all(self,date2):
		sqlQ="SELECT operational.calculate_stats_all(extract( epoch from timestamp with time zone %s)::bigint)"
		with self.conn.cursor() as cur:
			try:
				
				cur.execute(sqlQ,(date2.strftime('%Y-%m-%d 00:00:00+01'),))
				self.conn.commit()

			except Exception as err:
				print("Error calculating all stats for "+str(date2.strftime('%Y-%m-%d')))
				print(err)
				self.conn.rollback()

	def calculate_stats_urban(self,date2):
		sqlQ="SELECT operational.calculate_stats_urban(extract( epoch from timestamp with time zone %s )::bigint)"
		with self.conn.cursor() as cur:
			try:
				
				cur.execute(sqlQ,(date2.strftime('%Y-%m-%d 00:00:00+01'),))
				self.conn.commit()

			except Exception as err:
				print("Error calculating urban stats for "+str(date2.strftime('%Y-%m-%d ')))
				print(err)
				self.conn.rollback()

	def calculate_stats_suburban(self,date2):
		sqlQ="SELECT operational.calculate_stats_suburban(extract( epoch from timestamp with time zone %s )::bigint)"
		with self.conn.cursor() as cur:
			try:
				
				cur.execute(sqlQ,(date2.strftime('%Y-%m-%d 00:00:00+01'),))
				self.conn.commit()

			except Exception as err:
				print("Error calculating suburban stats for "+str(date2.strftime('%Y-%m-%d ')))
				print(err)
				self.conn.rollback()

	def calculate_stats_rural(self,date2):
		sqlQ="SELECT operational.calculate_stats_rural(extract( epoch from timestamp with time zone %s )::bigint)"
		with self.conn.cursor() as cur:
			try:
				
				cur.execute(sqlQ,(date2.strftime('%Y-%m-%d 00:00:00+01'),))
				self.conn.commit()

			except Exception as err:
				print("Error calculating rural stats for "+str(date2.strftime('%Y-%m-%d ')))
				print(err)
				self.conn.rollback()

	def calculate_stats_traffic(self,date2):
		sqlQ="SELECT operational.calculate_stats_traffic(extract( epoch from timestamp with time zone %s )::bigint)"
		with self.conn.cursor() as cur:
			try:
				
				cur.execute(sqlQ,(date2.strftime('%Y-%m-%d 00:00:00+01'),))
				self.conn.commit()

			except Exception as err:
				print("Error calculating traffic stats for "+str(date2.strftime('%Y-%m-%d ')))
				print(err)
				self.conn.rollback()				

	def calculate_stats_industrial(self,date2):
		sqlQ="SELECT operational.calculate_stats_industrial(extract( epoch from timestamp with time zone %s )::bigint)"
		with self.conn.cursor() as cur:
			try:
				
				cur.execute(sqlQ,(date2.strftime('%Y-%m-%d 00:00:00+01'),))
				self.conn.commit()

			except Exception as err:
				print("Error calculating industrial stats for "+str(date2.strftime('%Y-%m-%d ')))
				print(err)
				self.conn.rollback()
	'''***************************************
	REPORTING
	***************************************'''
	def report_industrial_errors(self,date2=None):
		#Returns report on industrial stations from given day.
		#if no date given - the latest avialable 
		if date2==None:
			sqlQ="select st.station_name,paramname, me,mae,rmsd,nrmsd,cov,cor,to_timestamp(unixdate) \
			from operational.error_stats_industrial err \
			join operational.stations st on st.id_station = err.id_station \
			where unixdate = (select max(unixdate) from operational.error_stats_industrial) \
			order by st.id_station,paramname"
			with self.conn.cursor() as cur:
				try:
					cur.execute(sqlQ)
					df=pd.DataFrame(cur.fetchall())
					df.columns=['station_name','paramname','me','mae','rmsd','nrmsd','cov','cor','date']
					return df
				except Exception as err:
					print("Error obtaining report")
					print(err)
		else:
			sqlQ="select st.station_name,paramname, me,mae,rmsd,nrmsd,cov,cor,to_timestamp(unixdate) \
				from operational.error_stats_industrial err \
				join operational.stations st on st.id_station = err.id_station \
				where unixdate = %s \
				order by st.id_station,paramname"

	def report_rural_errors(self,date2=None):
		#Returns report on industrial stations from given day.
		#if no date given - the latest avialable 
		if date2==None:
			sqlQ="select st.station_name,paramname, me,mae,rmsd,nrmsd,cov,cor,to_timestamp(unixdate) \
			from operational.error_stats_bcg_rural err \
			join operational.stations st on st.id_station = err.id_station \
			where unixdate = (select max(unixdate) from operational.error_stats_bcg_rural) \
			order by st.id_station,paramname"
			with self.conn.cursor() as cur:
				try:
					cur.execute(sqlQ)
					df=pd.DataFrame(cur.fetchall())
					df.columns=['station_name','paramname','me','mae','rmsd','nrmsd','cov','cor','date']
					return df
				except Exception as err:
					print("Error obtaining report")
					print(err)
		else:
			sqlQ="select st.station_name,paramname, me,mae,rmsd,nrmsd,cov,cor,to_timestamp(unixdate) \
				from operational.error_stats_bcg_rural err \
				join operational.stations st on st.id_station = err.id_station \
				where unixdate = %s \
				order by st.id_station,paramname"