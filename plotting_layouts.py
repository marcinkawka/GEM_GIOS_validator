import plotly.graph_objs as go

xaxis1=dict(
    title='time',
    rangeselector=dict(
            buttons=list([
                dict(count=24,
                     label='24h',
                     step='hour',
                     stepmode='todate'),
                dict(count=48,
                     label='48h',
                     step='hour',
                     stepmode='todate'),                     
                dict(count=7,
                     label='7d',
                     step='day',
                     stepmode='todate'),
                dict(count=1,
                     label='1m',
                     step='month',
                     stepmode='todate'),
                dict(step='all')
            ])
        ),
        rangeslider=dict(
            visible = True
        ),
        type='date'
)
yaxis_PM10={
    'nticks': 5, 
    'tick0': 0,
    'range':[0, 150],
    'autorange': False,
    'title':'Concentration'
}
layout_ts_PM10=go.Layout(xaxis=xaxis1,yaxis=yaxis_PM10,autosize=True,title="PM10 timeseries")
yaxis_NO2=yaxis_PM10
yaxis_NO2['range']=[0,80]
layout_ts_NO2=go.Layout(xaxis=xaxis1,yaxis=yaxis_NO2,autosize=True)

yaxis_SO2=yaxis_PM10
yaxis_SO2['range']=[0,45]
layout_ts_SO2=go.Layout(xaxis=xaxis1,yaxis=yaxis_SO2,autosize=True)

yaxis_O3=yaxis_PM10
yaxis_O3['range']=[0,100]
layout_ts_O3=go.Layout(xaxis=xaxis1,yaxis=yaxis_O3,autosize=True)

layout_histo = go.Layout(barmode='group',
                        width=500,
                        height=500,
                        autosize=True)

layout_geo = go.Layout(
    title = "TITLE WAS NOT SET!",
    font=dict(family='Courier New, monospace', size=18, color='rgb(0,0,0)'),
    autosize=True,
    hovermode='closest',
    showlegend=False,
    
    width=1000,
    height=800,
    mapbox=dict(
        accesstoken='pk.eyJ1Ijoid29keXBvZHByZXNqYSIsImEiOiJwYTIyV1ZrIn0.S6Ntk_5tiO-z4wXEV3d7zw',
        bearing=0,
        center=dict(
            lat=52,
            lon=20
        ),
        pitch=0,
        zoom=5,
        style = 'light'
    ),
)

c_pm10='rgb(31, 119, 180)'
c_no2='rgb(255, 127, 14)'
c_so2='rgb(44, 160, 44)'
c_o3='rgb(214, 39, 40)'

