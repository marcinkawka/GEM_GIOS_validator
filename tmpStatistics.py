#!/usr/bin/python3
import time
import pprint
from db_interface import DBConnection
from netCDF4 import Dataset
import numpy as np
from datetime import date, timedelta

def main():
    
	
    db=DBConnection()
    db.connect()
    for i in range(1,30):
        day = date.today() - timedelta(i)
        db.calculate_stats_traffic(day)
        db.calculate_stats_all(day)
        db.calculate_stats_industrial(day)
        db.calculate_stats_urban(day)
        db.calculate_stats_suburban(day)
        db.calculate_stats_rural(day)
        
def others():
    db.calculate_stats_all(yesterday)
    db.calculate_stats_industrial(yesterday)
    db.calculate_stats_urban(yesterday)
    db.calculate_stats_suburban(yesterday)
    db.calculate_stats_rural(yesterday)


if __name__=="__main__":
	main()