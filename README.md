Quick and dirty application prototype with the following features:
- download monitoring data from GIOS API
- store them in SQLite / PostgreSQL

TODO:
- extract data from RPN files and stroe in PostgreSQL db
- calculate several fancy validation functions (GEM vs. GIOS), like RMSE, R2,...
- visualize results on the web, using pyDash and Flask
