from db_interface import DBConnection
from netCDF4 import Dataset
import numpy as np

def find_nearest_idx(array, value):
	'''Function returns idx of nearest value in array
	it is usefull for searching for model's lat/long '''

	array = np.asarray(array)
	idx = (np.abs(array - value)).argmin()
	return idx

def find_GEM_station_coordinates(st_lat,st_long):
	'''Function finds neares coordinates within GEM mesh for a given station lat/long'''
	input_dir='/mnt/disk1/home/prognoza/pl25/diag_out/diag_conc_nc/20190101/'
	file4=Dataset(input_dir+"20190101_gemaq_file_4.nc","r",format="NETCDF3_CLASSIC")
	vars4 =file4.variables
	dims4 =file4.dimensions
	
	lon= np.array(vars4['lon'])
	lat= np.array(vars4['lat'])
	
	s_idx_lat=find_nearest_idx(lat,st_lat)
	s_idx_lon=find_nearest_idx(lon,st_long)

	return (lat[s_idx_lat],lon[s_idx_lon],s_idx_lat,s_idx_lon)

def main():
	db=DBConnection()
	db.connect()
	stations = db.getStations()
	for st in stations:
		
		st_id=st[0]
		st_lat=float(st[3])
		st_long=float(st[4])
		st_code=str(st[5])
		
		GEM_lat, GEM_long, GEM_idx_lat,GEM_idx_lon=find_GEM_station_coordinates(st_lat,st_long)
		try:
			db.storeValidationStation((st_id,st_code,GEM_lat,GEM_long,GEM_idx_lat,GEM_idx_lon))
		
		except Exception as e:
			print("Error saving station")
			print(e)

if __name__ =="__main__":
	main()