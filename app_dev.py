import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
from plotly import tools

import pandas as pd
import numpy as np
import sqlite3
import math

import pprint

from plotting_datasets import *
from plotting_graphs import *
from plotting_layouts import *
from plot_rendering import *


'''********************************************
script main body
********************************************'''    




pp = pprint.PrettyPrinter(indent=4)


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)



header = html.H1('GEM-AQ vs GIOŚ measurements validation')
content_div = html.Div(id='content_canvas_div')

tabs =  dcc.Tabs(id="tabs-main", children=[
            dcc.Tab(label='All stations', value='all', children=[
                dcc.Tabs(id='tabs-all',children=[
                    dcc.Tab(label='Timeseries comparison', value='all_ts'),
                    dcc.Tab(label='Scatter plots', value='all_scp'),
                    dcc.Tab(label='Error histograms', value='all_hist'),
                    dcc.Tab(label='Error spatial distribution', value='all_dist')
                ])
            ]),
            dcc.Tab(label='Urban backgroud', value='urban', children=[
                dcc.Tabs(id='tabs-urban',children=[
                    dcc.Tab(label='Timeseries comparison', value='urban_ts'),
                    dcc.Tab(label='Scatter plots', value='urban_scp'),
                    dcc.Tab(label='Error histograms', value='urban_hist'),
                    dcc.Tab(label='Error spatial distribution', value='urban_dist')
                ])
            ]),
            dcc.Tab(label='Suburban backgroud', value='suburban', children=[
                dcc.Tabs(id='tabs-suburban',children=[
                    dcc.Tab(label='Timeseries comparison', value='suburban_ts'),
                    dcc.Tab(label='Scatter plots', value='suburban_scp'),
                    dcc.Tab(label='Error histograms', value='suburban_hist'),
                    dcc.Tab(label='Error spatial distribution', value='suburban_dist')
                ])
            ]),
            dcc.Tab(label='Rural backgroud', value='rural', children=[
                dcc.Tabs(id='tabs-rural',children=[
                    dcc.Tab(label='Timeseries comparison', value='rural_ts'),
                    dcc.Tab(label='Scatter plots', value='rural_scp'),
                    dcc.Tab(label='Error histograms', value='rural_hist'),
                    dcc.Tab(label='Error spatial distribution', value='rural_dist')
                ])
            ]),
            dcc.Tab(label='Industrial', value='industrial', children=[
                dcc.Tabs(id='tabs-industrial',children=[
                    dcc.Tab(label='Timeseries comparison', value='industrial_ts'),
                    dcc.Tab(label='Scatter plots', value='industrial_scp'),
                    dcc.Tab(label='Error histograms', value='industrial_hist'),
                    dcc.Tab(label='Error spatial distribution', value='industrial_dist')
                ])
            ]),
            dcc.Tab(label='Traffic', value='traffic', children=[
                dcc.Tabs(id='tabs-traffic',children=[
                    dcc.Tab(label='Timeseries comparison', value='traffic_ts'),
                    dcc.Tab(label='Scatter plots', value='traffic_scp'),
                    dcc.Tab(label='Error histograms', value='traffic_hist'),
                    dcc.Tab(label='Error spatial distribution', value='traffic_dist')
                ])
            ])
        ])


app.layout = html.Div([
    header,
    tabs,
    content_div
])


@app.callback(Output('content_canvas_div', 'children'),
			[Input('tabs-main', 'value'),
            Input('tabs-all', 'value'),
            Input('tabs-urban', 'value'),
            Input('tabs-suburban', 'value'),
            Input('tabs-rural', 'value'),
            Input('tabs-industrial', 'value'),
            Input('tabs-traffic', 'value')])


def render_content(tabs_main,tabs_all,tabs_urban,tabs_suburban,
    tabs_rural,tabs_industrial,tabs_traffic):
    if tabs_all and tabs_main in tabs_all:
        return render_tab(tabs_all)   
    elif tabs_urban and tabs_main in tabs_urban:
        return render_tab(tabs_urban)
    elif tabs_suburban and tabs_main in tabs_suburban:
        return render_tab(tabs_suburban)
    elif tabs_rural and tabs_main in tabs_rural:
        return render_tab(tabs_rural)
    elif tabs_industrial and tabs_main in tabs_industrial:
        return render_tab(tabs_industrial)
    elif tabs_traffic and tabs_main in tabs_traffic:
        return render_tab(tabs_traffic)            

if __name__ == '__main__':
    app.run_server(debug=True,host='localhost',port=8051)