from plotting_datasets import *
import dash_table as dt0
import dash_table_experiments as dt

import dash
import dash_core_components as dcc
import dash_html_components as html

COLORS = [
    {
        'background': '#fef0d9',
        'text': 'rgb(30, 30, 30)'
    },
    {
        'background': '#fdcc8a',
        'text': 'rgb(30, 30, 30)'
    },
    {
        'background': '#fc8d59',
        'text': 'rgb(30, 30, 30)'
    },
    {
        'background': '#d7301f',
        'text': 'rgb(30, 30, 30)'
    },
]

def is_numeric(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


def cell_style(value, min_value, max_value):
    style = {}
    if is_numeric(value):
        relative_value = (float(value) - min_value) / (max_value - min_value)
        if relative_value <= 0.25:
            style = {
                'backgroundColor': COLORS[0]['background'],
                'color': COLORS[0]['text']
            }
        elif relative_value <= 0.5:
            style = {
                'backgroundColor': COLORS[1]['background'],
                'color': COLORS[1]['text']
            }
        elif relative_value <= 0.75:
            style = {
                'backgroundColor': COLORS[2]['background'],
                'color': COLORS[2]['text']
            }
        elif relative_value <= 1:
            style = {
                'backgroundColor': COLORS[3]['background'],
                'color': COLORS[3]['text']
            }
    return style


def ConditionalTable(dataframe1):
    max_value = df_industrial_report.max(numeric_only=True).max()
    min_value = df_industrial_report.min(numeric_only=True).max()
    min_value = 0
    max_value = 100
    rows = []
    for i in range(len(dataframe1)):
        row = {}
        for col in dataframe1.columns:
            value = dataframe1.iloc[i][col]
            #style = cell_style(value, min_value, max_value)
            row[col] = value
            #Div does not work -> no idea why?
            #row[col] = html.Div(
            #    value,
            #    style=dict({
            #        'height': '100%'
            #    }, **style)
            #)
        rows.append(row)
    return rows

def render_err_report(st_type):
    
    str_info = 'Error statistics for 24h of forecast starting on '
    
    if st_type=='industrial':
        date_str = str(df_industrial_report['date'].unique()[0]).split(' ')[0]
        
        return str_info+date_str+" 01:00 CET",dt.DataTable(
            rows=ConditionalTable(df_industrial_report),
            #rows=df_industrial_report.to_dict('records'),
            # optional - sets the order of columns
            columns=(df_industrial_report.columns),
            editable=False,
            column_widths = [300,100,100,100,100,100,100,100],
            min_width = 1200,
            min_height = 700,
            filterable=True,
            sortable=True,
            #selected_row_indices=[], (only in experimental version)
            id='datatable-gapminder'
        )
    elif st_type=='rural':
        
        date_str = str(df_rural_report['date'].unique()[0]).split(' ')[0]
        return str_info+date_str+" 01:00 CET",dt.DataTable(
            rows=ConditionalTable(df_rural_report),
            #rows=df_industrial_report.to_dict('records'),
            # optional - sets the order of columns
            columns=(df_rural_report.columns),
            editable=False,
            column_widths = [300,100,100,100,100,100,100,100],
            min_width = 1200,
            min_height = 700,
            filterable=True,
            sortable=True,
            #selected_row_indices=[], (only in experimental version)
            id='datatable-gapminder'
        )