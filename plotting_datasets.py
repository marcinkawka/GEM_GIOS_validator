from datetime import date, timedelta
import pandas as pd
from db_interface import DBConnection

new_year=1546300800

start_ts=new_year #+24*60*60*13
end_ts = start_ts +24*60*60*30

db=DBConnection()
db.connect()
'''
Getting data from db
'''
#Timeseries

df_PM10=db.getPdAggregates('PM10','all',start_ts,end_ts)
df_PM10_urban=db.getPdAggregates('PM10','urban',start_ts,end_ts)
df_PM10_suburban=db.getPdAggregates('PM10','suburban',start_ts,end_ts)
df_PM10_rural=db.getPdAggregates('PM10','rural',start_ts,end_ts)
df_PM10_traffic=db.getPdAggregates('PM10','traffic',start_ts,end_ts)
df_PM10_industrial=db.getPdAggregates('PM10','industrial',start_ts,end_ts)

df_NO2=db.getPdAggregates('NO2','all',start_ts,end_ts)
df_NO2_urban=db.getPdAggregates('NO2','urban',start_ts,end_ts)
df_NO2_suburban=db.getPdAggregates('NO2','suburban',start_ts,end_ts)
df_NO2_rural=db.getPdAggregates('NO2','rural',start_ts,end_ts)
df_NO2_traffic=db.getPdAggregates('NO2','traffic',start_ts,end_ts)
df_NO2_industrial=db.getPdAggregates('NO2','industrial',start_ts,end_ts)

df_SO2=db.getPdAggregates('SO2','all',start_ts,end_ts)
df_SO2_urban=db.getPdAggregates('SO2','urban',start_ts,end_ts)
df_SO2_suburban=db.getPdAggregates('SO2','suburban',start_ts,end_ts)
df_SO2_rural=db.getPdAggregates('SO2','rural',start_ts,end_ts)
df_SO2_traffic=db.getPdAggregates('SO2','traffic',start_ts,end_ts)
df_SO2_industrial=db.getPdAggregates('SO2','industrial',start_ts,end_ts)

df_O3=db.getPdAggregates('O3','all',start_ts,end_ts)
df_O3_urban=db.getPdAggregates('O3','urban',start_ts,end_ts)
df_O3_suburban=db.getPdAggregates('O3','suburban',start_ts,end_ts)
df_O3_rural=db.getPdAggregates('O3','rural',start_ts,end_ts)
df_O3_industrial=db.getPdAggregates('O3','industrial',start_ts,end_ts)

#Error statistics

df_PM10_errors_all =db.getPdLatestErrors('PM10','all')
df_PM10_errors_urban =db.getPdLatestErrors('PM10','urban')
df_PM10_errors_suburban =db.getPdLatestErrors('PM10','suburban')
df_PM10_errors_rural =db.getPdLatestErrors('PM10','rural')
df_PM10_errors_industrial =db.getPdLatestErrors('PM10','industrial')
df_PM10_errors_traffic =db.getPdLatestErrors('PM10','traffic')

df_NO2_errors_all =db.getPdLatestErrors('NO2','all')
df_NO2_errors_urban =db.getPdLatestErrors('NO2','urban')
df_NO2_errors_suburban =db.getPdLatestErrors('NO2','suburban')
df_NO2_errors_rural =db.getPdLatestErrors('NO2','rural')
df_NO2_errors_industrial =db.getPdLatestErrors('NO2','industrial')
df_NO2_errors_traffic =db.getPdLatestErrors('NO2','traffic')

df_SO2_errors_all =db.getPdLatestErrors('SO2','all')
df_SO2_errors_urban =db.getPdLatestErrors('SO2','urban')
df_SO2_errors_suburban =db.getPdLatestErrors('SO2','suburban')
df_SO2_errors_rural =db.getPdLatestErrors('SO2','rural')
df_SO2_errors_industrial =db.getPdLatestErrors('SO2','industrial')
df_SO2_errors_traffic =db.getPdLatestErrors('SO2','traffic')

df_O3_errors_all =db.getPdLatestErrors('O3','all')
df_O3_errors_urban =db.getPdLatestErrors('O3','urban')
df_O3_errors_suburban =db.getPdLatestErrors('O3','suburban')
df_O3_errors_rural =db.getPdLatestErrors('O3','rural')
df_O3_errors_industrial =db.getPdLatestErrors('O3','industrial')

df_industrial_report=db.report_industrial_errors()
df_rural_report=db.report_rural_errors()

# Others
df_PM10['t']=pd.to_datetime(df_PM10['unixdate'],unit='s')
t=df_PM10['t']

yesterday = date.today() - timedelta(1)
df_number=db.getNumberOfStations(yesterday)
df_number['st_type']='None'
msk=((df_number['station_type']=='tło') & (df_number['area_type']=='miejski'))
df_number.loc[ msk,'st_type']='urban'

df_stations = pd.DataFrame(db.getStations())
df_stations.columns=['id_station','address','street','lat','long','station_code']

msk=((df_number['station_type']=='tło') & (df_number['area_type']=='podmiejski'))
df_number.loc[ msk,'st_type']='suburban'

msk=((df_number['station_type']=='tło') & (df_number['area_type']=='pozamiejski'))
df_number.loc[ msk,'st_type']='rural'

df_number.loc[df_number['station_type']=='komunikacyjna','st_type']='traffic'
df_number.loc[df_number['station_type']=='przemysłowa','st_type']='industrial'

st_count_PM10={}
st_count_NO2={}
st_count_SO2={}
st_count_O3={}

st_count_PM10["all"]=(df_number[df_number['paramname']=='PM10']['liczba_stacji'].sum())
st_count_NO2["all"]=(df_number[df_number['paramname']=='NO2']['liczba_stacji'].sum())
st_count_SO2["all"]=(df_number[df_number['paramname']=='SO2']['liczba_stacji'].sum())
st_count_O3["all"]=(df_number[df_number['paramname']=='O3']['liczba_stacji'].sum())


msk=(df_number['paramname']=='PM10') & (df_number['st_type']=='traffic')
st_count_PM10["traffic"]=df_number[msk]['liczba_stacji'].sum()

msk=(df_number['paramname']=='NO2') & (df_number['st_type']=='traffic')
st_count_NO2["traffic"]=df_number[msk]['liczba_stacji'].sum()

msk=(df_number['paramname']=='SO2') & (df_number['st_type']=='traffic')
st_count_SO2["traffic"]=df_number[msk]['liczba_stacji'].sum()


msk=(df_number['paramname']=='PM10') & (df_number['st_type']=='urban')
st_count_PM10["urban"]=(df_number[msk]['liczba_stacji'].sum())
msk=(df_number['paramname']=='NO2') & (df_number['st_type']=='urban')
st_count_NO2["urban"]=(df_number[msk]['liczba_stacji'].sum())
msk=(df_number['paramname']=='SO2') & (df_number['st_type']=='urban')
st_count_SO2["urban"]=(df_number[msk]['liczba_stacji'].sum())
msk=(df_number['paramname']=='O3') & (df_number['st_type']=='urban')
st_count_O3["urban"]=(df_number[msk]['liczba_stacji'].sum())
    
msk=(df_number['paramname']=='PM10') & (df_number['st_type']=='suburban')
st_count_PM10["suburban"]=(df_number[msk]['liczba_stacji'].sum())
msk=(df_number['paramname']=='NO2') & (df_number['st_type']=='suburban')
st_count_NO2["suburban"]=(df_number[msk]['liczba_stacji'].sum())
msk=(df_number['paramname']=='SO2') & (df_number['st_type']=='suburban')
st_count_SO2["suburban"]=(df_number[msk]['liczba_stacji'].sum())
msk=(df_number['paramname']=='O3') & (df_number['st_type']=='suburban')
st_count_O3["suburban"]=(df_number[msk]['liczba_stacji'].sum())

msk=(df_number['paramname']=='PM10') & (df_number['st_type']=='rural')
st_count_PM10["rural"]=(df_number[msk]['liczba_stacji'].sum())
msk=(df_number['paramname']=='NO2') & (df_number['st_type']=='rural')
st_count_NO2["rural"]=(df_number[msk]['liczba_stacji'].sum())
msk=(df_number['paramname']=='SO2') & (df_number['st_type']=='rural')
st_count_SO2["rural"]=(df_number[msk]['liczba_stacji'].sum())
msk=(df_number['paramname']=='O3') & (df_number['st_type']=='rural')
st_count_O3["rural"]=(df_number[msk]['liczba_stacji'].sum())

msk=((df_number['paramname']=='PM10') & (df_number['st_type']=='industrial'))
st_count_PM10["industrial"]=(df_number[msk]['liczba_stacji'].sum())
msk=((df_number['paramname']=='NO2') & (df_number['st_type']=='industrial'))
st_count_NO2["industrial"]=(df_number[msk]['liczba_stacji'].sum())
msk=((df_number['paramname']=='SO2') & (df_number['st_type']=='industrial'))
st_count_SO2["industrial"]=(df_number[msk]['liczba_stacji'].sum())
msk=((df_number['paramname']=='O3') & (df_number['st_type']=='industrial'))
st_count_O3["industrial"]=(df_number[msk]['liczba_stacji'].sum())