
from plotting_datasets import *

scl=[ [0.0,"rgb(75, 174, 94)"],[0.125,"rgb(102, 188, 85)"],[0.25,"rgb(222, 222, 99)"],[0.5,"rgb(255, 206, 8)"],[1,"rgb(250, 75, 60)"]]

spd_PM10_all=[dict(
        type = 'scattermapbox',
        lat= df_PM10_errors_all['station_lat'],
        lon= df_PM10_errors_all['station_long'],
        text = df_PM10_errors_all['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_PM10_errors_all['nrmsd']*100,
            cmax=df_PM10_errors_all['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      
spd_PM10_urban=[dict(
        type = 'scattermapbox',
        lat= df_PM10_errors_urban['station_lat'],
        lon= df_PM10_errors_urban['station_long'],     
        text = df_PM10_errors_urban['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_PM10_errors_urban['nrmsd']*100,
            cmax=df_PM10_errors_urban['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_PM10_suburban=[dict(
        type = 'scattermapbox',
        lat= df_PM10_errors_suburban['station_lat'],
        lon= df_PM10_errors_suburban['station_long'],     
        text = df_PM10_errors_suburban['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_PM10_errors_suburban['nrmsd']*100,
            cmax=df_PM10_errors_suburban['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_PM10_rural=[dict(
        type = 'scattermapbox',
        lat= df_PM10_errors_rural['station_lat'],
        lon= df_PM10_errors_rural['station_long'],     
        text = df_PM10_errors_rural['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_PM10_errors_rural['nrmsd']*100,
            cmax=df_PM10_errors_rural['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_PM10_traffic=[dict(
        type = 'scattermapbox',
        lat= df_PM10_errors_traffic['station_lat'],
        lon= df_PM10_errors_traffic['station_long'],     
        text = df_PM10_errors_traffic['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_PM10_errors_traffic['nrmsd']*100,
            cmax=df_PM10_errors_traffic['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_PM10_industrial=[dict(
        type = 'scattermapbox',
        lat= df_PM10_errors_industrial['station_lat'],
        lon= df_PM10_errors_industrial['station_long'],     
        text = df_PM10_errors_industrial['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_PM10_errors_industrial['nrmsd']*100,
            cmax=df_PM10_errors_industrial['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      
''' #############################################
    NO2
################################################'''


spd_NO2_all=[dict(
        type = 'scattermapbox',
        lat= df_NO2_errors_all['station_lat'],
        lon= df_NO2_errors_all['station_long'],
        text = df_NO2_errors_all['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_NO2_errors_all['nrmsd']*100,
            cmax=df_NO2_errors_all['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      
spd_NO2_urban=[dict(
        type = 'scattermapbox',
        lat= df_NO2_errors_urban['station_lat'],
        lon= df_NO2_errors_urban['station_long'],     
        text = df_NO2_errors_urban['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_NO2_errors_urban['nrmsd']*100,
            cmax=df_NO2_errors_urban['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_NO2_suburban=[dict(
        type = 'scattermapbox',
        lat= df_NO2_errors_suburban['station_lat'],
        lon= df_NO2_errors_suburban['station_long'],     
        text = df_NO2_errors_suburban['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_NO2_errors_suburban['nrmsd']*100,
            cmax=df_NO2_errors_suburban['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_NO2_rural=[dict(
        type = 'scattermapbox',
        lat= df_NO2_errors_rural['station_lat'],
        lon= df_NO2_errors_rural['station_long'],     
        text = df_NO2_errors_rural['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_NO2_errors_rural['nrmsd']*100,
            cmax=df_NO2_errors_rural['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_NO2_traffic=[dict(
        type = 'scattermapbox',
        lat= df_NO2_errors_traffic['station_lat'],
        lon= df_NO2_errors_traffic['station_long'],     
        text = df_NO2_errors_traffic['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_NO2_errors_traffic['nrmsd']*100,
            cmax=df_NO2_errors_traffic['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_NO2_industrial=[dict(
        type = 'scattermapbox',
        lat= df_NO2_errors_industrial['station_lat'],
        lon= df_NO2_errors_industrial['station_long'],     
        text = df_NO2_errors_industrial['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_NO2_errors_industrial['nrmsd']*100,
            cmax=df_NO2_errors_industrial['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      
''' #############################################
    SO2
################################################'''


spd_SO2_all=[dict(
        type = 'scattermapbox',
        lat= df_SO2_errors_all['station_lat'],
        lon= df_SO2_errors_all['station_long'],
        text = df_SO2_errors_all['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_SO2_errors_all['nrmsd']*100,
            cmax=df_SO2_errors_all['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      
spd_SO2_urban=[dict(
        type = 'scattermapbox',
        lat= df_SO2_errors_urban['station_lat'],
        lon= df_SO2_errors_urban['station_long'],     
        text = df_SO2_errors_urban['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_SO2_errors_urban['nrmsd']*100,
            cmax=df_SO2_errors_urban['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_SO2_suburban=[dict(
        type = 'scattermapbox',
        lat= df_SO2_errors_suburban['station_lat'],
        lon= df_SO2_errors_suburban['station_long'],     
        text = df_SO2_errors_suburban['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_SO2_errors_suburban['nrmsd']*100,
            cmax=df_SO2_errors_suburban['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_SO2_rural=[dict(
        type = 'scattermapbox',
        lat= df_SO2_errors_rural['station_lat'],
        lon= df_SO2_errors_rural['station_long'],     
        text = df_SO2_errors_rural['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_SO2_errors_rural['nrmsd']*100,
            cmax=df_SO2_errors_rural['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_SO2_traffic=[dict(
        type = 'scattermapbox',
        lat= df_SO2_errors_traffic['station_lat'],
        lon= df_SO2_errors_traffic['station_long'],     
        text = df_SO2_errors_traffic['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_SO2_errors_traffic['nrmsd']*100,
            cmax=df_SO2_errors_traffic['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_SO2_industrial=[dict(
        type = 'scattermapbox',
        lat= df_SO2_errors_industrial['station_lat'],
        lon= df_SO2_errors_industrial['station_long'],     
        text = df_SO2_errors_industrial['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_SO2_errors_industrial['nrmsd']*100,
            cmax=df_SO2_errors_industrial['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      
''' #############################################
    O3
################################################'''


spd_O3_all=[dict(
        type = 'scattermapbox',
        lat= df_O3_errors_all['station_lat'],
        lon= df_O3_errors_all['station_long'],
        text = df_O3_errors_all['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_O3_errors_all['nrmsd']*100,
            cmax=df_O3_errors_all['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      
spd_O3_urban=[dict(
        type = 'scattermapbox',
        lat= df_O3_errors_urban['station_lat'],
        lon= df_O3_errors_urban['station_long'],     
        text = df_O3_errors_urban['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_O3_errors_urban['nrmsd']*100,
            cmax=df_O3_errors_urban['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_O3_suburban=[dict(
        type = 'scattermapbox',
        lat= df_O3_errors_suburban['station_lat'],
        lon= df_O3_errors_suburban['station_long'],     
        text = df_O3_errors_suburban['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_O3_errors_suburban['nrmsd']*100,
            cmax=df_O3_errors_suburban['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_O3_rural=[dict(
        type = 'scattermapbox',
        lat= df_O3_errors_rural['station_lat'],
        lon= df_O3_errors_rural['station_long'],     
        text = df_O3_errors_rural['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_O3_errors_rural['nrmsd']*100,
            cmax=df_O3_errors_rural['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      

spd_O3_industrial=[dict(
        type = 'scattermapbox',
        lat= df_O3_errors_industrial['station_lat'],
        lon= df_O3_errors_industrial['station_long'],     
        text = df_O3_errors_industrial['nrmsd']*100, 
        mode='markers',
        marker = dict(
            size=10,
            opacity = 1,
            autocolorscale = False,
            reversescale = False,
            colorscale=scl,
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),            
            color=df_O3_errors_industrial['nrmsd']*100,
            cmax=df_O3_errors_industrial['nrmsd']*100,
            cmin=0.0,
            colorbar=dict(
                title="NRMSD [%]"
            )
        ),
        hoverinfo='text'
)]                      
spd_PM10={'all':spd_PM10_all,'urban':spd_PM10_urban,'suburban':spd_PM10_suburban,
    'rural':spd_PM10_rural,'industrial':spd_PM10_industrial,'traffic':spd_PM10_traffic}

spd_NO2={'all':spd_NO2_all,'urban':spd_NO2_urban,'suburban':spd_NO2_suburban,
    'rural':spd_NO2_rural,'industrial':spd_NO2_industrial,'traffic':spd_NO2_traffic}

spd_SO2={'all':spd_SO2_all,'urban':spd_SO2_urban,'suburban':spd_SO2_suburban,
    'rural':spd_SO2_rural,'industrial':spd_SO2_industrial,'traffic':spd_SO2_traffic}

spd_O3={'all':spd_O3_all,'urban':spd_O3_urban,'suburban':spd_O3_suburban,
    'rural':spd_O3_rural,'industrial':spd_O3_industrial}