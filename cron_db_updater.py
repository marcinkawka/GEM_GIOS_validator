import GIOS_API
import time

from db_interface import DBConnection
import urllib.request, json 
import urllib.error as error


'''skrypt do updatowania bazy za pomocą crona'''

def main():
	downloadAndStoreLatestResults()

def downloadAndStoreLatestResults():
	'''Iterates through all devices from db'''
	try:
		db=DBConnection()
		db.connect()
		devs = db.getDevices()
		db=None
	except Exception as e:
		print("Error while getting devices list")
		print(e)
		return
	
	try:		
		for dev in devs:
			downloadAndStoreDeviceResults(dev[0])
			time.sleep(2)
	except:
		print("Error while storing results")

def downloadAndStoreDeviceResults(devID):
	'''download and store results of selected device'''
	res=GIOS_API.download_results(devID)
	db=DBConnection()
	db.connect()
	db.deleteNullResults()
	db.storeResults(res)
	db=None

	
if __name__ =="__main__":
	main()