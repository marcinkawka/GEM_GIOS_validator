﻿insert into operational.gios_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
    select paramname,unix_date as unixdate,
        avg(NULLIF(res.value,'NaN')),median(NULLIF(res.value,'NaN')),stddev_samp(NULLIF(res.value,'NaN')),
        min(NULLIF(res.value,'NaN')),max(NULLIF(res.value,'NaN')),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.1),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.25),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.75),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.90)
    from operational.results res 
        join operational.measurements m on m.id_measurement=res.id_measurement 
        where res.unix_date not in(
            select unixdate from operational.gios_aggregates
        )
    group by unixdate, paramname
    order by unix_date desc;

   /* MIEJSKIE */
insert into operational.gios_bcg_urban_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
     select paramname, unix_date as unixdate,
        avg(NULLIF(res.value,'NaN')),median(NULLIF(res.value,'NaN')),stddev_samp(NULLIF(res.value,'NaN')),
        min(NULLIF(res.value,'NaN')),max(NULLIF(res.value,'NaN')),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.1),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.25),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.75),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.90)
    from operational.results res 
        join operational.measurements m on m.id_measurement=res.id_measurement 
        join operational.stations st on m.id_station=st.id_station
    where st.station_type='tło' and st.area_type='miejski'
        and res.unix_date not in(
            select unixdate from operational.gios_bcg_urban_aggregates
                        )
group by unixdate, paramname
order by unixdate desc;

   /* PODMIEJSKIE */
insert into operational.gios_bcg_suburban_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
     select paramname, unix_date as unixdate,
        avg(NULLIF(res.value,'NaN')),median(NULLIF(res.value,'NaN')),stddev_samp(NULLIF(res.value,'NaN')),
        min(NULLIF(res.value,'NaN')),max(NULLIF(res.value,'NaN')),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.1),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.25),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.75),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.90)
    from operational.results res 
        join operational.measurements m on m.id_measurement=res.id_measurement 
        join operational.stations st on m.id_station=st.id_station
    where st.station_type='tło' and st.area_type='podmiejski'
        and res.unix_date not in(
            select unixdate from operational.gios_bcg_suburban_aggregates
                        )
group by unixdate, paramname
order by unixdate desc;

   /* POZAMIEJSKIE */
insert into operational.gios_bcg_rural_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
     select paramname, unix_date as unixdate,
        avg(NULLIF(res.value,'NaN')),median(NULLIF(res.value,'NaN')),stddev_samp(NULLIF(res.value,'NaN')),
        min(NULLIF(res.value,'NaN')),max(NULLIF(res.value,'NaN')),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.1),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.25),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.75),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.90)
    from operational.results res 
        join operational.measurements m on m.id_measurement=res.id_measurement 
        join operational.stations st on m.id_station=st.id_station
    where st.station_type='tło' and st.area_type='pozamiejski'
        and res.unix_date not in(
            select unixdate from operational.gios_bcg_rural_aggregates
                        )
group by unixdate, paramname
order by unixdate desc;

   /* KOMUNIKACYJNA */
insert into operational.gios_traffic_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
     select paramname, unix_date as unixdate,
        avg(NULLIF(res.value,'NaN')),median(NULLIF(res.value,'NaN')),stddev_samp(NULLIF(res.value,'NaN')),
        min(NULLIF(res.value,'NaN')),max(NULLIF(res.value,'NaN')),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.1),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.25),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.75),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.90)
    from operational.results res 
        join operational.measurements m on m.id_measurement=res.id_measurement 
        join operational.stations st on m.id_station=st.id_station
    where st.station_type='komunikacyjna' and st.area_type='miejski'
        and res.unix_date not in(
            select unixdate from operational.gios_traffic_aggregates
                        )
group by unixdate, paramname
order by unixdate desc;

   /* PRZEMYSŁOWA */
insert into operational.gios_industrial_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
     select paramname, unix_date as unixdate,
        avg(NULLIF(res.value,'NaN')),median(NULLIF(res.value,'NaN')),stddev_samp(NULLIF(res.value,'NaN')),
        min(NULLIF(res.value,'NaN')),max(NULLIF(res.value,'NaN')),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.1),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.25),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.75),
        operational.percentile_cont(array_agg(NULLIF(res.value,'NaN')), 0.90)
    from operational.results res 
        join operational.measurements m on m.id_measurement=res.id_measurement 
        join operational.stations st on m.id_station=st.id_station
    where st.station_type='przemysłowa' and st.area_type='miejski'
        and res.unix_date not in(
            select unixdate from operational.gios_industrial_aggregates
                        )
group by unixdate, paramname
order by unixdate desc;
