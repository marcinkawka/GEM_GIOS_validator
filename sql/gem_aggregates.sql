﻿insert into operational.gem_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
    select paramname, unixdate, 
    avg(value),median(value),stddev_samp(value) as stddev,
    min(value),max(value),
    operational.percentile_cont(array_agg(value), 0.1),
    operational.percentile_cont(array_agg(value), 0.25),
    operational.percentile_cont(array_agg(value), 0.75),
    operational.percentile_cont(array_agg(value), 0.90)
    from operational.gem_results
    where unixdate not in(
        select unixdate from operational.gem_aggregates
                        )
    group by unixdate, paramname;

/* POZAMIEJSKIE */
insert into operational.gem_bcg_rural_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
     select paramname, unixdate,
	avg(value),median(value),stddev_samp(value) as stddev,
 min(value),max(value),
    operational.percentile_cont(array_agg(value), 0.1),
    operational.percentile_cont(array_agg(value), 0.25),
    operational.percentile_cont(array_agg(value), 0.75),
    operational.percentile_cont(array_agg(value), 0.90)
    from operational.gem_results res
    join operational.stations st on res.id_station=st.id_station
    where st.station_type='tło' and st.area_type='pozamiejski'
        and unixdate not in(
            select unixdate from operational.gem_bcg_rural_aggregates
                        )
group by unixdate, paramname
order by unixdate desc;

/* PODMIEJSKIE */
insert into operational.gem_bcg_suburban_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
     select paramname, unixdate,
	avg(value),median(value),stddev_samp(value) as stddev,
 min(value),max(value),
    operational.percentile_cont(array_agg(value), 0.1),
    operational.percentile_cont(array_agg(value), 0.25),
    operational.percentile_cont(array_agg(value), 0.75),
    operational.percentile_cont(array_agg(value), 0.90)
    from operational.gem_results res
    join operational.stations st on res.id_station=st.id_station
    where st.station_type='tło' and st.area_type='podmiejski'
        and unixdate not in(
            select unixdate from operational.gem_bcg_suburban_aggregates
                        )
group by unixdate, paramname
order by unixdate desc;

/* MIEJSKIE */
insert into operational.gem_bcg_urban_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
     select paramname, unixdate,
	avg(value),median(value),stddev_samp(value) as stddev,
 min(value),max(value),
    operational.percentile_cont(array_agg(value), 0.1),
    operational.percentile_cont(array_agg(value), 0.25),
    operational.percentile_cont(array_agg(value), 0.75),
    operational.percentile_cont(array_agg(value), 0.90)
    from operational.gem_results res
    join operational.stations st on res.id_station=st.id_station
    where st.station_type='tło' and st.area_type='miejski'
        and unixdate not in(
            select unixdate from operational.gem_bcg_urban_aggregates
                        )
group by unixdate, paramname
order by unixdate desc;

/* KOMUNIKACYJNE */
insert into operational.gem_traffic_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
     select paramname, unixdate,
	avg(value),median(value),stddev_samp(value) as stddev,
 min(value),max(value),
    operational.percentile_cont(array_agg(value), 0.1),
    operational.percentile_cont(array_agg(value), 0.25),
    operational.percentile_cont(array_agg(value), 0.75),
    operational.percentile_cont(array_agg(value), 0.90)
    from operational.gem_results res
    join operational.stations st on res.id_station=st.id_station
    where st.station_type='komunikacyjna' and st.area_type='miejski'
        and unixdate not in(
            select unixdate from operational.gem_traffic_aggregates
                        )
group by unixdate, paramname
order by unixdate desc;

/* PRZEMYSŁOWE */
insert into operational.gem_industrial_aggregates(paramname,unixdate,avg,median,stddev,min,max,p10,p25,p75,p90)
     select paramname, unixdate,
	avg(value),median(value),stddev_samp(value) as stddev,
 min(value),max(value),
    operational.percentile_cont(array_agg(value), 0.1),
    operational.percentile_cont(array_agg(value), 0.25),
    operational.percentile_cont(array_agg(value), 0.75),
    operational.percentile_cont(array_agg(value), 0.90)
    from operational.gem_results res
    join operational.stations st on res.id_station=st.id_station
    where st.station_type='przemysłowa' and st.area_type='miejski'
        and unixdate not in(
            select unixdate from operational.gem_industrial_aggregates
                        )
group by unixdate, paramname
order by unixdate desc;