select to_timestamp(err2.unixdate) as date,
	err2.paramname,
	err2.nrmsd*100 as NRMSD,
	st.station_name 
from operational.error_stats_all err2
join operational.stations st 
	on st.id_station = err2.id_station
where (err2.nrmsd,err2.unixdate,err2.paramname) in (
	select min(err1.nrmsd) as min_rmsd,
		err1.unixdate,err1.paramname
	from operational.error_stats_all err1
	where unixdate=(
		select max(unixdate) from operational.error_stats_all
	) 
	group by paramname,unixdate
) 