﻿/* WSZYSTKIE STACJE */
insert into operational.error_stats_all(paramname,id_station,unixdate,mae,me,rmsd,cov,cor)
    select paramname,id_station,extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01'),
    operational.calculate_mae(array_agg(model_value),array_agg(monitoring_value)) as mae,
    operational.calculate_me(array_agg(model_value),array_agg(monitoring_value)) as me,
    operational.calculate_rmsd(array_agg(model_value),array_agg(monitoring_value)) as rmsd,
    covar_samp(model_value,monitoring_value) as cov,
    corr(model_value,monitoring_value) as cor
    from v_comparison 
    where unixdate >= extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01')
    and unixdate < extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01' + interval '1 days')
    group by id_station,paramname;
    

/* TŁO MIEJSKIE */
insert into operational.error_stats_bcg_urban(paramname,id_station,unixdate,mae,me,rmsd,cov,cor)
    select paramname,id_station,extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01'),
    operational.calculate_mae(array_agg(model_value),array_agg(monitoring_value)) as mae,
    operational.calculate_me(array_agg(model_value),array_agg(monitoring_value)) as me,
    operational.calculate_rmsd(array_agg(model_value),array_agg(monitoring_value)) as rmsd,
    covar_samp(model_value,monitoring_value) as cov,
    corr(model_value,monitoring_value) as cor
    from v_comparison 
    where unixdate >= extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01')
    and unixdate < extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01' + interval '1 days')
    and station_type='tło'
    and area_type='miejski'    
    group by id_station,paramname;

/* TŁO PODMIEJSKIE */
insert into operational.error_stats_bcg_suburban(paramname,id_station,unixdate,mae,me,rmsd,cov,cor)
    select paramname,id_station,extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01'),
    operational.calculate_mae(array_agg(model_value),array_agg(monitoring_value)) as mae,
    operational.calculate_me(array_agg(model_value),array_agg(monitoring_value)) as me,
    operational.calculate_rmsd(array_agg(model_value),array_agg(monitoring_value)) as rmsd,
    covar_samp(model_value,monitoring_value) as cov,
    corr(model_value,monitoring_value) as cor
    from v_comparison 
    where unixdate >= extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01')
    and unixdate < extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01' + interval '1 days')
    and station_type='tło'
    and area_type='podmiejski'    
    group by id_station,paramname;

/* TŁO POZAMIEJSKIE */
insert into operational.error_stats_bcg_rural(paramname,id_station,unixdate,mae,me,rmsd,cov,cor)
    select paramname,id_station,extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01'),
    operational.calculate_mae(array_agg(model_value),array_agg(monitoring_value)) as mae,
    operational.calculate_me(array_agg(model_value),array_agg(monitoring_value)) as me,
    operational.calculate_rmsd(array_agg(model_value),array_agg(monitoring_value)) as rmsd,
    covar_samp(model_value,monitoring_value) as cov,
    corr(model_value,monitoring_value) as cor
    from v_comparison 
    where unixdate >= extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01')
    and unixdate < extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01' + interval '1 days')
    and station_type='tło'
    and area_type='pozamiejski'    
    group by id_station,paramname;

/* KOMUNIKACYJNE */
insert into operational.error_stats_traffic(paramname,id_station,unixdate,mae,me,rmsd,cov,cor)
    select paramname,id_station,extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01'),
    operational.calculate_mae(array_agg(model_value),array_agg(monitoring_value)) as mae,
    operational.calculate_me(array_agg(model_value),array_agg(monitoring_value)) as me,
    operational.calculate_rmsd(array_agg(model_value),array_agg(monitoring_value)) as rmsd,
    covar_samp(model_value,monitoring_value) as cov,
    corr(model_value,monitoring_value) as cor
    from v_comparison 
    where unixdate >= extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01')
    and unixdate < extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01' + interval '1 days')
    and station_type='komunikacyjna'
    group by id_station,paramname;

/* PRZEMYSŁOWE */
insert into operational.error_stats_industrial(paramname,id_station,unixdate,mae,me,rmsd,cov,cor)
    select paramname,id_station,extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01'),
    operational.calculate_mae(array_agg(model_value),array_agg(monitoring_value)) as mae,
    operational.calculate_me(array_agg(model_value),array_agg(monitoring_value)) as me,
    operational.calculate_rmsd(array_agg(model_value),array_agg(monitoring_value)) as rmsd,
    covar_samp(model_value,monitoring_value) as cov,
    corr(model_value,monitoring_value) as cor
    from v_comparison 
    where unixdate >= extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01')
    and unixdate < extract( epoch from timestamp with time zone '2019-01-28 00:00:00+01' + interval '1 days')
    and station_type='przemysłowa'
    group by id_station,paramname;        