﻿SELECT paramname, err.id_station, st.station_name,
	round(operational.percentile_cont(array_agg(cor),0.25)::numeric,2) as Quantile_25,
	round(avg(cor),2) as avg_cor,
	round(operational.percentile_cont(array_agg(cor),0.5)::numeric,2)  as median,
	round(operational.percentile_cont(array_agg(cor),0.75)::numeric,2) as Quantile_75,
       round(avg(mes_number)/24*100,1) as completness
  FROM operational.error_stats_all err join operational.stations st on st.id_station = err.id_station
where unixdate >= extract( epoch from timestamp '2019-01-01')
and unixdate < extract( epoch from timestamp '2019-02-01')
and paramname='PM10'
group by paramname, err.id_station,st.station_name  
order by avg_cor desc
limit 5;

SELECT paramname, err.id_station, st.station_name,
	round(operational.percentile_cont(array_agg(cor),0.25)::numeric,2) as Quantile_25,
	round(avg(cor),2) as avg_cor,
	round(operational.percentile_cont(array_agg(cor),0.5)::numeric,2)  as median,
	round(operational.percentile_cont(array_agg(cor),0.75)::numeric,2) as Quantile_75,
       round(avg(mes_number)/24*100,1) as completness
  FROM operational.error_stats_all err join operational.stations st on st.id_station = err.id_station
where unixdate >= extract( epoch from timestamp '2019-01-01')
and unixdate < extract( epoch from timestamp '2019-02-01')
and paramname='NO2'
group by paramname, err.id_station,st.station_name  
order by avg_cor desc
limit 5;

SELECT paramname, err.id_station, st.station_name,
	round(operational.percentile_cont(array_agg(cor),0.25)::numeric,2) as Quantile_25,
	round(avg(cor),2) as avg_cor,
	round(operational.percentile_cont(array_agg(cor),0.5)::numeric,2)  as median,
	round(operational.percentile_cont(array_agg(cor),0.75)::numeric,2) as Quantile_75,
       round(avg(mes_number)/24*100,1) as completness
  FROM operational.error_stats_all err join operational.stations st on st.id_station = err.id_station
where unixdate >= extract( epoch from timestamp '2019-01-01')
and unixdate < extract( epoch from timestamp '2019-02-01')
and paramname='SO2'
group by paramname, err.id_station,st.station_name  
order by avg_cor desc
limit 5;

SELECT paramname, err.id_station, st.station_name,
	round(operational.percentile_cont(array_agg(cor),0.25)::numeric,2) as Quantile_25,
	round(avg(cor),2) as avg_cor,
	round(operational.percentile_cont(array_agg(cor),0.5)::numeric,2)  as median,
	round(operational.percentile_cont(array_agg(cor),0.75)::numeric,2) as Quantile_75,
       round(avg(mes_number)/24*100,1) as completness
  FROM operational.error_stats_all err join operational.stations st on st.id_station = err.id_station
where unixdate >= extract( epoch from timestamp '2019-01-01')
and unixdate < extract( epoch from timestamp '2019-02-01')
and paramname='O3'
group by paramname, err.id_station,st.station_name  
order by avg_cor desc
limit 5;

--- NRMSD

SELECT paramname, err.id_station, st.station_name,
	round(operational.percentile_cont(array_agg(nrmsd),0.25)::numeric,2)*100 as Quantile_25,
	round(avg(nrmsd),2)*100 as avg_nrmsd,
	round(operational.percentile_cont(array_agg(nrmsd),0.5)::numeric,2)*100  as median,
	round(operational.percentile_cont(array_agg(nrmsd),0.75)::numeric,2)*100 as Quantile_75,
       round(avg(mes_number)/24*100,1) as completness
  FROM operational.error_stats_all err join operational.stations st on st.id_station = err.id_station
where unixdate >= extract( epoch from timestamp '2019-01-01')
and unixdate < extract( epoch from timestamp '2019-02-01')
and paramname='PM10'
group by paramname, err.id_station,st.station_name  
order by avg_nrmsd asc
limit 5;

SELECT paramname, err.id_station, st.station_name,
	round(operational.percentile_cont(array_agg(nrmsd),0.25)::numeric,2)*100 as Quantile_25,
	round(avg(nrmsd),2)*100 as avg_nrmsd,
	round(operational.percentile_cont(array_agg(nrmsd),0.5)::numeric,2)*100  as median,
	round(operational.percentile_cont(array_agg(nrmsd),0.75)::numeric,2)*100 as Quantile_75,
       round(avg(mes_number)/24*100,1) as completness
  FROM operational.error_stats_all err join operational.stations st on st.id_station = err.id_station
where unixdate >= extract( epoch from timestamp '2019-01-01')
and unixdate < extract( epoch from timestamp '2019-02-01')
and paramname='NO2'
group by paramname, err.id_station,st.station_name  
order by avg_nrmsd asc
limit 5;

SELECT paramname, err.id_station, st.station_name,
	round(operational.percentile_cont(array_agg(nrmsd),0.25)::numeric,2)*100 as Quantile_25,
	round(avg(nrmsd),2)*100 as avg_nrmsd,
	round(operational.percentile_cont(array_agg(nrmsd),0.5)::numeric,2)*100  as median,
	round(operational.percentile_cont(array_agg(nrmsd),0.75)::numeric,2)*100 as Quantile_75,
       round(avg(mes_number)/24*100,1) as completness
  FROM operational.error_stats_all err join operational.stations st on st.id_station = err.id_station
where unixdate >= extract( epoch from timestamp '2019-01-01')
and unixdate < extract( epoch from timestamp '2019-02-01')
and paramname='SO2'
group by paramname, err.id_station,st.station_name  
order by avg_nrmsd asc
limit 5;

SELECT paramname, err.id_station, st.station_name,
	round(operational.percentile_cont(array_agg(nrmsd),0.25)::numeric,2)*100 as Quantile_25,
	round(avg(nrmsd),2)*100 as avg_nrmsd,
	round(operational.percentile_cont(array_agg(nrmsd),0.5)::numeric,2)*100  as median,
	round(operational.percentile_cont(array_agg(nrmsd),0.75)::numeric,2)*100 as Quantile_75,
       round(avg(mes_number)/24*100,1) as completness
  FROM operational.error_stats_all err join operational.stations st on st.id_station = err.id_station
where unixdate >= extract( epoch from timestamp '2019-01-01')
and unixdate < extract( epoch from timestamp '2019-02-01')
and paramname='O3'
group by paramname, err.id_station,st.station_name  
order by avg_nrmsd asc
limit 8;
