import urllib.request, json 
import pandas as pd
import sqlite3
import pprint
import numpy as np
import time

#Wrapper do API GIOS


stations_url="http://api.gios.gov.pl/pjp-api/rest/station/findAll"
dev_url="http://api.gios.gov.pl/pjp-api/rest/station/sensors/" #+stationID 
data_url="http://api.gios.gov.pl/pjp-api/rest/data/getData/" #+devID
index_url="http://api.gios.gov.pl/pjp-api/rest/aqindex/getIndex/"#+stationID

def download_station_list():
	'''downloads station list from GIOS API'''
	with urllib.request.urlopen(stations_url) as url:
		data = json.loads(url.read().decode())
	#data is a JSON object
	df=pd.DataFrame(data)
	df2=df[['stationName','addressStreet','id','gegrLat','gegrLon']]
	return list(df2.to_records(index=False))


def download_sensors_list(station_id):
	s_url=dev_url+str(station_id)
	
	res=[]
		
	with urllib.request.urlopen(s_url) as url:
		data = json.loads(url.read().decode())

		for d in data:
			id_measurement =d['id']
			id_station=d['stationId']
			paramName=d['param']['paramCode']
			res.append((id_measurement,id_station,paramName))
	return res

def download_results(devID):
	# downloads measurements of selected deviceID
	d_url=data_url+str(devID)
	
	with urllib.request.urlopen(d_url) as url:
		data = json.loads(url.read().decode())
		
		try:
			df=pd.DataFrame(data['values'])	


			df['date2']=pd.DatetimeIndex(pd.to_datetime(df['date']).dt.tz_localize('CET'))
			del df['date']
			df.set_index('date2',inplace=True)
			df['unix_date']= ((df.index.astype(np.int64))/1000000000).astype(np.int64)
			df['id_measurement']=devID
			df.set_index('unix_date',inplace=True)
			
		except Exception as e:
			print("Error processing "+str(devID))
			print(e)

		return list(df.to_records())