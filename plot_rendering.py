import dash_core_components as dcc
import dash_html_components as html
import dash_table
from plotly import tools
import math
import numpy as np
import copy

from plotting_datasets import *
from plotting_graphs import *
from plotting_layouts import *
from plotting_maps import *
from plotting_reports import *

def roundup(x):
    return int(math.ceil(x / 10.0)+1) * 10

def getMaxRange(df):
    '''gets the optimal maximum for scatter plot axis range'''
    maxy = roundup(np.nanmax(df['y'].astype(np.float)))
    maxx = roundup(np.nanmax(df['x'].astype(np.float)))
    return max(maxx,maxy)


def render_scp(st_type):
    xaxis1= dict(
        title= 'GIOŚ measurement',
        ticklen= 5,
        zeroline= True,
        gridwidth= 2,
        range=[0 ,150],
    )
    yaxis1=dict(
        title= 'GEM-AQ model',
        ticklen= 5,
        zeroline= True,
        gridwidth= 2,
        range=[0 ,150],
    )
    layout1 = go.Layout(
        paper_bgcolor='rgb(255,255,255)',
        plot_bgcolor='rgb(229,229,229)')

    if st_type != 'traffic':
        fig = tools.make_subplots(rows=2, cols=2,
                                shared_xaxes=False, shared_yaxes=False)
        fig.append_trace(PM10_scatter[st_type], 1, 1)
        fig.append_trace(PM10_scatter['line'], 1, 1)
        
        fig.append_trace(NO2_scatter[st_type], 1, 2)
        fig.append_trace(NO2_scatter['line'], 1, 2)

        fig.append_trace(SO2_scatter[st_type], 2, 1)
        fig.append_trace(SO2_scatter['line'], 2, 1)

        fig.append_trace(O3_scatter[st_type], 2, 2)
        fig.append_trace(O3_scatter['line'], 2, 2)

        fig['layout']['xaxis1'].update(xaxis1,range=[0 ,getMaxRange(PM10_scatter[st_type])])
        fig['layout']['xaxis2'].update(xaxis1,range=[0 ,getMaxRange(NO2_scatter[st_type])])
        fig['layout']['xaxis3'].update(xaxis1,range=[0 ,getMaxRange(SO2_scatter[st_type])])
        fig['layout']['xaxis4'].update(xaxis1,range=[0 ,getMaxRange(O3_scatter[st_type])])
        
        fig['layout']['yaxis1'].update(yaxis1,range=[0 ,getMaxRange(PM10_scatter[st_type])])
        fig['layout']['yaxis2'].update(yaxis1,range=[0 ,getMaxRange(NO2_scatter[st_type])])
        fig['layout']['yaxis3'].update(yaxis1,range=[0 ,getMaxRange(SO2_scatter[st_type])])
        fig['layout']['yaxis4'].update(yaxis1,range=[0 ,getMaxRange(O3_scatter[st_type])])
        fig['layout'].update(height=800, width=800, title='Scatter plots ')
        
    else:
        fig = tools.make_subplots(rows=2, cols=2,
                                shared_xaxes=False, shared_yaxes=False)
        fig.append_trace(PM10_scatter[st_type], 1, 1)
        fig.append_trace(PM10_scatter['line'], 1, 1)

        fig.append_trace(NO2_scatter[st_type], 1, 2)
        fig.append_trace(NO2_scatter['line'], 1, 2)

        fig.append_trace(SO2_scatter[st_type], 2, 1)  
        fig.append_trace(SO2_scatter['line'], 2, 1)  

        fig['layout']['xaxis1'].update(xaxis1,range=[0 ,getMaxRange(PM10_scatter[st_type])])
        fig['layout']['xaxis2'].update(xaxis1,range=[0 ,getMaxRange(NO2_scatter[st_type])])
        fig['layout']['xaxis3'].update(xaxis1,range=[0 ,getMaxRange(SO2_scatter[st_type])])
        fig['layout']['yaxis1'].update(yaxis1,range=[0 ,getMaxRange(PM10_scatter[st_type])])
        fig['layout']['yaxis2'].update(yaxis1,range=[0 ,getMaxRange(NO2_scatter[st_type])])
        fig['layout']['yaxis3'].update(yaxis1,range=[0 ,getMaxRange(SO2_scatter[st_type])])
        
        fig['layout'].update(height=800, width=800,  title='Scatter plots ')
    
    return fig

def render_histo(st_type,err_type):

    fig = tools.make_subplots(rows=2, cols=2,shared_xaxes=False, shared_yaxes=False)
    if err_type=='pm10':
        fig.append_trace(PM10_histo_mae[st_type], 1, 1)
        fig.append_trace(PM10_histo_me[st_type], 1, 2)
        fig.append_trace(PM10_histo_rmsd[st_type], 2, 1)
        fig.append_trace(PM10_histo_cor[st_type], 2, 2)        
    elif err_type=='no2':
        fig.append_trace(NO2_histo_mae[st_type], 1, 1)
        fig.append_trace(NO2_histo_me[st_type], 1, 2)
        fig.append_trace(NO2_histo_rmsd[st_type], 2, 1)
        fig.append_trace(NO2_histo_cor[st_type], 2, 2)        
    elif err_type=='so2':
        fig.append_trace(SO2_histo_mae[st_type], 1, 1)
        fig.append_trace(SO2_histo_me[st_type], 1, 2)
        fig.append_trace(SO2_histo_rmsd[st_type], 2, 1)
        fig.append_trace(SO2_histo_cor[st_type], 2, 2)
    elif err_type=='o3' and st_type!='traffic':
        fig.append_trace(O3_histo_mae[st_type], 1, 1)
        fig.append_trace(O3_histo_me[st_type], 1, 2)
        fig.append_trace(O3_histo_rmsd[st_type], 2, 1)
        fig.append_trace(O3_histo_cor[st_type], 2, 2)        
    fig['layout'].update(height=800, width=800, title='Error Histograms ')
    return fig


def render_maps(st_type):
    
    title_PM10 = "24H NORMALISED RMSD FOR PM10 "+str(st_type).upper()+" STATIONS ON "+str(df_PM10_errors_all['date'][0]).split(" ")[0]
    layout_PM10=copy.deepcopy(layout_geo)
    layout_PM10['title']=title_PM10
    fig = dict( data=spd_PM10[st_type], layout=layout_PM10 )
    g_PM10=dcc.Graph(figure=fig, id='map_PM10_'+st_type)

    title_NO2 = "24H NORMALISED RMSD FOR NO2 "+str(st_type).upper()+" STATIONS ON "+str(df_NO2_errors_all['date'][0]).split(" ")[0]
    layout_NO2=copy.deepcopy(layout_geo)
    layout_NO2['title']=title_NO2
    fig = dict( data=spd_NO2[st_type], layout=layout_NO2 )
    g_NO2=dcc.Graph(figure=fig, id='map_NO2_'+st_type)
    
    title_SO2 = "24H NORMALISED RMSD FOR SO2 "+str(st_type).upper()+" STATIONS ON "+str(df_SO2_errors_all['date'][0]).split(" ")[0]
    layout_SO2=copy.deepcopy(layout_geo)
    layout_SO2['title']=title_SO2
    fig = dict( data=spd_SO2[st_type], layout=layout_SO2 )
    g_SO2=dcc.Graph(figure=fig, id='map_SO2_'+st_type)
    
    if st_type =='traffic':
        return html.Div([g_PM10,g_NO2,g_SO2])
    else:
        title_O3 = "24H NORMALISED RMSD FOR O3 "+str(st_type).upper()+" STATIONS ON "+str(df_O3_errors_all['date'][0]).split(" ")[0]
        layout_O3=copy.deepcopy(layout_geo)
        layout_O3['title']=title_O3
        fig = dict( data=spd_O3[st_type], layout=layout_O3)
        g_O3=dcc.Graph(figure=fig, id='map_O3_'+st_type)
        return html.Div([g_PM10,g_NO2,g_SO2,g_O3])

def render_tab(tab_name):
    ''' This function is called when the top-level tab is selected
    st_type - [all,urban,suburban,industrial,traffic]
    vis_type - [ts, scp, hist, dist]
    '''
    st_type=tab_name.split("_")[0]
    vis_type=tab_name.split("_")[1]
    str_info ="Generated, based on "
    if vis_type == 'ts':
        if st_type == 'traffic':
            return html.Div([
                html.H3('PM10 '+str(st_type)+' stations'),graph_PM10[st_type],
                str_info+str(st_count_PM10[st_type])+" "+str(st_type)+" stations",
                
                html.H3('NO2 '+str(st_type)+' stations'),graph_NO2[st_type],
                str_info+str(st_count_NO2[st_type])+" "+str(st_type)+" stations",

                html.H3('SO2 '+str(st_type)+' stations'),graph_SO2[st_type],                                  
                str_info+str(st_count_SO2[st_type])+" "+str(st_type)+" stations",
                
            ])
        else:
             return html.Div([
                html.H3('PM10 '+str(st_type)+' stations'),graph_PM10[st_type],
                str_info+str(st_count_PM10[st_type])+" "+str(st_type)+" stations",

                html.H3('NO2 '+str(st_type)+' stations'),graph_NO2[st_type],
                str_info+str(st_count_NO2[st_type])+" "+str(st_type)+" stations",

                html.H3('SO2 '+str(st_type)+' stations'),graph_SO2[st_type],                                  
                str_info+str(st_count_SO2[st_type])+" "+str(st_type)+" stations",

                html.H3('O3 '+str(st_type)+' stations'),graph_O3[st_type],
                str_info+str(st_count_O3[st_type])+" "+str(st_type)+" stations",                                                                    
            ])           
    elif vis_type == 'scp':
        return html.Div([
            html.H3('Scatter plot '+str(st_type)+' stations'),
            dcc.Graph(figure=render_scp(st_type), id='my-figure')         
        ])    
    elif vis_type == 'hist' and (st_type == 'industrial' or st_type== 'rural'):
        #return "Too few stations to draw histogram"    
        return render_err_report(st_type)
    elif vis_type == 'hist':
        return html.Div([
            "Statystyki błędów dotyczą ostatniej pełnej doby z prognozą (zwykle wczoraj), uaktualniane o 5CET",
            
            html.H3('PM10 errors for '+str(st_type)+' stations'),
            dcc.Graph(figure=render_histo(st_type,'pm10'), id='pm10-hist'),           
            html.H3('NO2 errors for '+str(st_type)+' stations'),
            dcc.Graph(figure=render_histo(st_type,'no2'), id='no2-hist'),           
            html.H3('SO2 errors for '+str(st_type)+' stations'),
            dcc.Graph(figure=render_histo(st_type,'so2'), id='so2-hist'),           
            html.H3('O3 errors for '+str(st_type)+' stations'),
            dcc.Graph(figure=render_histo(st_type,'o3'), id='o3-hist')
        ])        
    elif vis_type == 'dist':
        return render_maps(st_type)

'''            html.H3('ME (Mean Error) histogram '+str(st_type)+' stations'),
            dcc.Graph(figure=render_histo(st_type,'me'), id='me-hist'),  
            html.H3('MAE (Mean Absolute Error) histograms '+str(st_type)+' stations'),
            dcc.Graph(figure=render_histo(st_type,'mae'), id='mae-hist'),
            html.H3('RMSD (Root Mean Square Deviation) histograms '+str(st_type)+' stations'),
            dcc.Graph(figure=render_histo(st_type,'rmsd'), id='rmsd-hist'),
            html.H3('Correlation coeficient histograms '+str(st_type)+' stations'),
            dcc.Graph(figure=render_histo(st_type,'cor'), id='corr-hist')    
''' 