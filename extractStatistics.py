#!/usr/bin/python3
import time
import pprint
from db_interface import DBConnection
from netCDF4 import Dataset
import numpy as np
from datetime import date, timedelta

def main():
    yesterday = date.today() - timedelta(1)
	
    db=DBConnection()
    db.connect()
    db.calculate_stats_all(yesterday)
    db.calculate_stats_industrial(yesterday)
    db.calculate_stats_urban(yesterday)
    db.calculate_stats_suburban(yesterday)
    db.calculate_stats_rural(yesterday)
    db.calculate_stats_traffic(yesterday)
    

if __name__=="__main__":
	main()