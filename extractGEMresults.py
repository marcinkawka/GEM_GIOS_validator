#!/usr/bin/python3
import time
import pprint
from db_interface import DBConnection
from netCDF4 import Dataset
import numpy as np
from datetime import date

#this file is ready for crontabization

def extractTS(idx_lat,idx_lon,param,t0):
	'''TODO:Move to separate module GEMTools'''
	'''Extract timeseries from GEM results file'''
	input_dir='/mnt/disk1/home/prognoza/pl25/diag_out/diag_conc_nc/'+str(t0)+'/'
	filename=str(t0)+"_gemaq_file_4.nc"

	file4=Dataset(input_dir+filename,"r",format="NETCDF3_CLASSIC")
	vars4 =file4.variables

	#48h to za długo
	#ts_length=vars4['PM10'].shape[0]
	ts_length=24
	
	ts=np.empty(ts_length)
	for i in range(ts_length): 
		ts[i]=vars4[param][i][0][idx_lat][idx_lon]

	return ts	

def extractAndStore(gem_stations,db,param,t0):
	
	for st in gem_stations:
		id_station=st[0]
		idx_lat=int(st[4])
		idx_lon=int(st[5])
		ts = extractTS(idx_lat,idx_lon,param,t0)
		db.storeGEMts1h(id_station,param,t0,ts)


def main():
	today = str(date.today().strftime('%Y%m%d'))

	db=DBConnection()
	db.connect()
	gem_stations = db.getGEMStations()

	for param in ['PM10','SO2','NO2','O3']:
		extractAndStore(gem_stations,db,param,today)


if __name__=="__main__":
	main()