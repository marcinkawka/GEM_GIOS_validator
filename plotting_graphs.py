import plotly.graph_objs as go
import dash_core_components as dcc
import pandas as pd
from plotting_datasets import *
from plotting_layouts import *

c_red='rgba(225,35,65,1)'
c_red_tr='rgba(225,35,65,0.2)'
c_blue='rgba(65,105,225,1)'
c_blue_tr='rgba(65,105,225,0.2)'
'''***********************************
Creating graph objects (go) - GIOŚ
*************************************'''
PM10_gios_ts={
    'all':go.Scatter(x=t,y=df_PM10['gios_avg'],name='PM10 GIOŚ',line=dict(color=c_red)),
    'urban':go.Scatter(x=t,y=df_PM10_urban['gios_avg'],name='PM10 GIOŚ',line=dict(color=c_red)),
    'suburban':go.Scatter(x=t,y=df_PM10_suburban['gios_avg'],name='PM10 GIOŚ',line=dict(color=c_red)),
    'traffic':go.Scatter(x=t,y=df_PM10_traffic['gios_avg'],name='PM10 GIOŚ',line=dict(color=c_red)),
    'rural':go.Scatter(x=t,y=df_PM10_rural['gios_avg'],name='PM10 GIOŚ',line=dict(color=c_red)),
    'industrial':go.Scatter(x=t,y=df_PM10_industrial['gios_avg'],name='PM10 GIOŚ',line=dict(color=c_red))
}
PM10_gios_p25={
    'all':go.Scatter(x=t,y=df_PM10['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_PM10_urban['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_PM10_suburban['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_PM10_traffic['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_PM10_rural['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_PM10_industrial['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False)
}
PM10_gios_p75={
    'all':go.Scatter(x=t,y=df_PM10['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_PM10_urban['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_PM10_suburban['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_PM10_traffic['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_PM10_rural['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_PM10_industrial['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False)
}

NO2_gios_ts={
    'all':go.Scatter(x=t,y=df_NO2['gios_avg'],name='NO2 GIOŚ',line=dict(color=c_red)),
    'urban':go.Scatter(x=t,y=df_NO2_urban['gios_avg'],name='NO2 GIOŚ',line=dict(color=c_red)),
    'suburban':go.Scatter(x=t,y=df_NO2_suburban['gios_avg'],name='NO2 GIOŚ',line=dict(color=c_red)),
    'traffic':go.Scatter(x=t,y=df_NO2_traffic['gios_avg'],name='NO2 GIOŚ',line=dict(color=c_red)),
    'rural':go.Scatter(x=t,y=df_NO2_rural['gios_avg'],name='NO2 GIOŚ',line=dict(color=c_red)),
    'industrial':go.Scatter(x=t,y=df_NO2_industrial['gios_avg'],name='NO2 GIOŚ',line=dict(color=c_red))
}
NO2_gios_p25={
    'all':go.Scatter(x=t,y=df_NO2['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_NO2_urban['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_NO2_suburban['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_NO2_traffic['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_NO2_rural['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_NO2_industrial['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False)
}
NO2_gios_p75={
    'all':go.Scatter(x=t,y=df_NO2['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_NO2_urban['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_NO2_suburban['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_NO2_traffic['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_NO2_rural['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_NO2_industrial['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False)
}


SO2_gios_ts={
    'all':go.Scatter(x=t,y=df_SO2['gios_avg'],name='SO2 GIOŚ',line=dict(color=c_red)),
    'urban':go.Scatter(x=t,y=df_SO2_urban['gios_avg'],name='SO2 GIOŚ',line=dict(color=c_red)),
    'suburban':go.Scatter(x=t,y=df_SO2_suburban['gios_avg'],name='SO2 GIOŚ',line=dict(color=c_red)),
    'traffic':go.Scatter(x=t,y=df_SO2_traffic['gios_avg'],name='SO2 GIOŚ',line=dict(color=c_red)),
    'rural':go.Scatter(x=t,y=df_SO2_rural['gios_avg'],name='SO2 GIOŚ',line=dict(color=c_red)),
    'industrial':go.Scatter(x=t,y=df_SO2_industrial['gios_avg'],name='SO2 GIOŚ',line=dict(color=c_red))
}
SO2_gios_p25={
    'all':go.Scatter(x=t,y=df_SO2['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_SO2_urban['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_SO2_suburban['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_SO2_traffic['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_SO2_rural['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_SO2_industrial['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False)
}
SO2_gios_p75={
    'all':go.Scatter(x=t,y=df_SO2['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_SO2_urban['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_SO2_suburban['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_SO2_traffic['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_SO2_rural['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_SO2_industrial['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False)
}


O3_gios_ts={
    'all':go.Scatter(x=t,y=df_O3['gios_avg'],name='O3 GIOŚ',line=dict(color=c_red)),
    'urban':go.Scatter(x=t,y=df_O3_urban['gios_avg'],name='O3 GIOŚ',line=dict(color=c_red)),
    'suburban':go.Scatter(x=t,y=df_O3_suburban['gios_avg'],name='O3 GIOŚ',line=dict(color=c_red)),
    'rural':go.Scatter(x=t,y=df_O3_rural['gios_avg'],name='O3 GIOŚ',line=dict(color=c_red)),
    'industrial':go.Scatter(x=t,y=df_O3_industrial['gios_avg'],name='O3 GIOŚ',line=dict(color=c_red))
}
O3_gios_p25={
    'all':go.Scatter(x=t,y=df_O3['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_O3_urban['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_O3_suburban['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_O3_rural['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_O3_industrial['gios_p25'],name='lower quartile',line=dict(color=c_red_tr),showlegend=False)
}
O3_gios_p75={
    'all':go.Scatter(x=t,y=df_O3['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_O3_urban['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_O3_suburban['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
     'rural':go.Scatter(x=t,y=df_O3_rural['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_O3_industrial['gios_p75'],name='upper quartile',line=dict(color=c_red_tr),showlegend=False)
}

'''***********************************
Creating graphs - GEM-AQ
*************************************'''
PM10_gem_ts={
    'all':go.Scatter(x=t,y=df_PM10['gem_avg'],name='PM10 GEM-AQ',line=dict(color=c_blue)),
    'urban':go.Scatter(x=t,y=df_PM10_urban['gem_avg'],name='PM10 GEM-AQ',line=dict(color=c_blue)),
    'suburban':go.Scatter(x=t,y=df_PM10_suburban['gem_avg'],name='PM10 GEM-AQ',line=dict(color=c_blue)),
    'traffic':go.Scatter(x=t,y=df_PM10_traffic['gem_avg'],name='PM10 GEM-AQ',line=dict(color=c_blue)),
    'rural':go.Scatter(x=t,y=df_PM10_rural['gem_avg'],name='PM10 GEM-AQ',line=dict(color=c_blue)),
    'industrial':go.Scatter(x=t,y=df_PM10_industrial['gem_avg'],name='PM10 GEM-AQ',line=dict(color=c_blue))
}
PM10_gem_p25={
    'all':go.Scatter(x=t,y=df_PM10['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_PM10_urban['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_PM10_suburban['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_PM10_traffic['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_PM10_rural['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_PM10_industrial['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False)
}
PM10_gem_p75={
    'all':go.Scatter(x=t,y=df_PM10['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_PM10_urban['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_PM10_suburban['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_PM10_traffic['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_PM10_rural['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_PM10_industrial['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False)
}

NO2_gem_ts={
    'all':go.Scatter(x=t,y=df_NO2['gem_avg'],name='NO2 GEM-AQ',line=dict(color=c_blue)),
    'urban':go.Scatter(x=t,y=df_NO2_urban['gem_avg'],name='NO2 GEM-AQ',line=dict(color=c_blue)),
    'suburban':go.Scatter(x=t,y=df_NO2_suburban['gem_avg'],name='NO2 GEM-AQ',line=dict(color=c_blue)),
    'traffic':go.Scatter(x=t,y=df_NO2_traffic['gem_avg'],name='NO2 GEM-AQ',line=dict(color=c_blue)),
    'rural':go.Scatter(x=t,y=df_NO2_rural['gem_avg'],name='NO2 GEM-AQ',line=dict(color=c_blue)),
    'industrial':go.Scatter(x=t,y=df_NO2_industrial['gem_avg'],name='NO2 GEM-AQ',line=dict(color=c_blue))
}
NO2_gem_p25={
    'all':go.Scatter(x=t,y=df_NO2['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_NO2_urban['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_NO2_suburban['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_NO2_traffic['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_NO2_rural['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_NO2_industrial['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False)
}
NO2_gem_p75={
    'all':go.Scatter(x=t,y=df_NO2['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_NO2_urban['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_NO2_suburban['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_NO2_traffic['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_NO2_rural['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_NO2_industrial['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False)
}

SO2_gem_ts={
    'all':go.Scatter(x=t,y=df_SO2['gem_avg'],name='SO2 GEM-AQ',line=dict(color=c_blue)),
    'urban':go.Scatter(x=t,y=df_SO2_urban['gem_avg'],name='SO2 GEM-AQ',line=dict(color=c_blue)),
    'suburban':go.Scatter(x=t,y=df_SO2_suburban['gem_avg'],name='SO2 GEM-AQ',line=dict(color=c_blue)),
    'traffic':go.Scatter(x=t,y=df_SO2_traffic['gem_avg'],name='SO2 GEM-AQ',line=dict(color=c_blue)),
    'rural':go.Scatter(x=t,y=df_SO2_rural['gem_avg'],name='SO2 GEM-AQ',line=dict(color=c_blue)),
    'industrial':go.Scatter(x=t,y=df_SO2_industrial['gem_avg'],name='SO2 GEM-AQ',line=dict(color=c_blue))
}
SO2_gem_p25={
    'all':go.Scatter(x=t,y=df_SO2['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_SO2_urban['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_SO2_suburban['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_SO2_traffic['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_SO2_rural['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_SO2_industrial['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False)
}
SO2_gem_p75={
    'all':go.Scatter(x=t,y=df_SO2['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_SO2_urban['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_SO2_suburban['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'traffic':go.Scatter(x=t,y=df_SO2_traffic['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_SO2_rural['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_SO2_industrial['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False)
}

O3_gem_ts={
    'all':go.Scatter(x=t,y=df_O3['gem_avg'],name='O3 GEM-AQ',line=dict(color=c_blue)),
    'urban':go.Scatter(x=t,y=df_O3_urban['gem_avg'],name='O3 GEM-AQ',line=dict(color=c_blue)),
    'suburban':go.Scatter(x=t,y=df_O3_suburban['gem_avg'],name='O3 GEM-AQ',line=dict(color=c_blue)),
    'rural':go.Scatter(x=t,y=df_O3_rural['gem_avg'],name='O3 GEM-AQ',line=dict(color=c_blue)),
    'industrial':go.Scatter(x=t,y=df_O3_industrial['gem_avg'],name='O3 GEM-AQ',line=dict(color=c_blue))
}
O3_gem_p25={
    'all':go.Scatter(x=t,y=df_O3['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_O3_urban['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_O3_suburban['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_O3_rural['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_O3_industrial['gem_p25'],name='lower quartile',line=dict(color=c_blue_tr),showlegend=False)
}
O3_gem_p75={
    'all':go.Scatter(x=t,y=df_O3['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'urban':go.Scatter(x=t,y=df_O3_urban['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'suburban':go.Scatter(x=t,y=df_O3_suburban['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'rural':go.Scatter(x=t,y=df_O3_rural['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False),
    'industrial':go.Scatter(x=t,y=df_O3_industrial['gem_p75'],name='upper quartile',line=dict(color=c_blue_tr),showlegend=False)
}
'''***********************************
Scatter plots
*************************************'''
PM10_scatter={
    'all':go.Scatter(x=df_PM10['gios_avg'],y=df_PM10['gem_avg'],name='PM10',mode = 'markers'),
    'urban':go.Scatter(x=df_PM10_urban['gios_avg'],y=df_PM10_urban['gem_avg'],name='PM10',mode = 'markers'),
    'suburban':go.Scatter(x=df_PM10_suburban['gios_avg'],y=df_PM10_suburban['gem_avg'],name='PM10',mode = 'markers'),
    'rural':go.Scatter(x=df_PM10_rural['gios_avg'],y=df_PM10_rural['gem_avg'],name='PM10',mode = 'markers'),
    'industrial':go.Scatter(x=df_PM10_industrial['gios_avg'],y=df_PM10_industrial['gem_avg'],name='PM10 ',mode = 'markers'),
    'traffic':go.Scatter(x=df_PM10_traffic['gios_avg'],y=df_PM10_traffic['gem_avg'],name='PM10 ',mode = 'markers',line = dict(color = c_pm10)),
    'line':go.Scatter(x=[-1000,1000],y=[-1000,1000],line = dict(
        color = c_pm10,
        width = 1),showlegend=False)
}

NO2_scatter={
    'all':go.Scatter(x=df_NO2['gios_avg'],y=df_NO2['gem_avg'],name='NO2 ',mode = 'markers',line = dict(color = c_no2)),
    'urban':go.Scatter(x=df_NO2_urban['gios_avg'],y=df_NO2_urban['gem_avg'],name='NO2 ',mode = 'markers',line = dict(color = c_no2)),
    'suburban':go.Scatter(x=df_NO2_suburban['gios_avg'],y=df_NO2_suburban['gem_avg'],name='NO2 ',mode = 'markers',line = dict(color = c_no2)),
    'rural':go.Scatter(x=df_NO2_rural['gios_avg'],y=df_NO2_rural['gem_avg'],name='NO2 ',mode = 'markers',line = dict(color = c_no2)),
    'industrial':go.Scatter(x=df_NO2_industrial['gios_avg'],y=df_NO2_industrial['gem_avg'],name='NO2 ',mode = 'markers',line = dict(color = c_no2)),
    'traffic':go.Scatter(x=df_NO2_traffic['gios_avg'],y=df_NO2_traffic['gem_avg'],name='NO2 ',mode = 'markers',line = dict(color = c_no2)),
        'line':go.Scatter(x=[-1000,1000],y=[-1000,1000],line = dict(
        color = c_no2,
        width = 1),showlegend=False)
}
SO2_scatter={
    'all':go.Scatter(x=df_SO2['gios_avg'],y=df_SO2['gem_avg'],name='SO2 ',mode = 'markers',line = dict(color = c_so2)),
    'urban':go.Scatter(x=df_SO2_urban['gios_avg'],y=df_SO2_urban['gem_avg'],name='SO2 ',mode = 'markers',line = dict(color = c_so2)),
    'suburban':go.Scatter(x=df_SO2_suburban['gios_avg'],y=df_SO2_suburban['gem_avg'],name='SO2 ',mode = 'markers',line = dict(color = c_so2)),
    'rural':go.Scatter(x=df_SO2_rural['gios_avg'],y=df_SO2_rural['gem_avg'],name='SO2 ',mode = 'markers',line = dict(color = c_so2)),
    'industrial':go.Scatter(x=df_SO2_industrial['gios_avg'],y=df_SO2_industrial['gem_avg'],name='SO2 ',mode = 'markers',line = dict(color = c_so2)),
    'traffic':go.Scatter(x=df_SO2_traffic['gios_avg'],y=df_SO2_traffic['gem_avg'],name='SO2 ',mode = 'markers',line = dict(color = c_so2)),
        'line':go.Scatter(x=[-1000,1000],y=[-1000,1000],line = dict(
        color = c_so2,
        width = 1),showlegend=False)
}
O3_scatter={
    'all':go.Scatter(x=df_O3['gios_avg'],y=df_O3['gem_avg'],name='O3 ',mode = 'markers',line = dict(color = c_o3)),
    'urban':go.Scatter(x=df_O3_urban['gios_avg'],y=df_O3_urban['gem_avg'],name='O3 ',mode = 'markers',line = dict(color = c_o3)),
    'suburban':go.Scatter(x=df_O3_suburban['gios_avg'],y=df_O3_suburban['gem_avg'],name='O3 ',mode = 'markers',line = dict(color = c_o3)),
    'rural':go.Scatter(x=df_O3_rural['gios_avg'],y=df_O3_rural['gem_avg'],name='O3 ',mode = 'markers',line = dict(color = c_o3)),
    'industrial':go.Scatter(x=df_O3_industrial['gios_avg'],y=df_O3_industrial['gem_avg'],name='O3 ',mode = 'markers',line = dict(color = c_o3)),
        'line':go.Scatter(x=[-1000,1000],y=[-1000,1000],line = dict(
        color = c_o3,
        width = 1),showlegend=False)
}
'''***********************************
Histograms - MAE
*************************************'''
PM10_histo_mae={
    'all':go.Histogram(x=df_PM10_errors_all['mae'], nbinsx = 10,name='Mean Absolute Error (MAE)'),
    'urban':go.Histogram(x=df_PM10_errors_urban['mae'],nbinsx = 10,name='Mean Absolute Error (MAE)'),
    'suburban':go.Histogram(x=df_PM10_errors_suburban['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)'),
    'rural':go.Histogram(x=df_PM10_errors_rural['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)'),
    'traffic':go.Histogram(x=df_PM10_errors_traffic['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)'),
    'industrial':go.Histogram(x=df_PM10_errors_industrial['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)')
}
NO2_histo_mae={
    'all':go.Histogram(x=df_NO2_errors_all['mae'], nbinsx = 10,name='Mean Absolute Error (MAE)'),
    'urban':go.Histogram(x=df_NO2_errors_urban['mae'],nbinsx = 10,name='Mean Absolute Error (MAE)'),
    'suburban':go.Histogram(x=df_NO2_errors_suburban['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)'),
    'rural':go.Histogram(x=df_NO2_errors_rural['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)'),
    'traffic':go.Histogram(x=df_NO2_errors_traffic['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)'),
    'industrial':go.Histogram(x=df_NO2_errors_industrial['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)')
}
SO2_histo_mae={
    'all':go.Histogram(x=df_SO2_errors_all['mae'], nbinsx = 10,name='Mean Absolute Error (MAE)'),
    'urban':go.Histogram(x=df_SO2_errors_urban['mae'],nbinsx = 10,name='Mean Absolute Error (MAE)'),
    'suburban':go.Histogram(x=df_SO2_errors_suburban['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)'),
    'rural':go.Histogram(x=df_SO2_errors_rural['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)'),
    'traffic':go.Histogram(x=df_SO2_errors_traffic['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)'),
    'industrial':go.Histogram(x=df_SO2_errors_industrial['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)')
}
O3_histo_mae={
    'all':go.Histogram(x=df_O3_errors_all['mae'], nbinsx = 10,name='Mean Absolute Error (MAE)'),
    'urban':go.Histogram(x=df_O3_errors_urban['mae'],nbinsx = 10,name='Mean Absolute Error (MAE)'),
    'suburban':go.Histogram(x=df_O3_errors_suburban['mae'],name='Mean Absolute Error (MAE)'),
    'rural':go.Histogram(x=df_O3_errors_rural['mae'],name='Mean Absolute Error (MAE)'),
#    'traffic':go.Histogram(x=df_O3_errors_traffic['mae'],nbinsx = 5), nie istnieje!
    'industrial':go.Histogram(x=df_O3_errors_industrial['mae'],nbinsx = 5,name='Mean Absolute Error (MAE)')
}
'''***********************************
Histograms - ME
*************************************'''
PM10_histo_me={
    'all':go.Histogram(x=df_PM10_errors_all['me'], nbinsx = 10,name='Mean Error (ME)'),
    'urban':go.Histogram(x=df_PM10_errors_urban['me'],nbinsx = 10,name='Mean Error (ME)'),
    'suburban':go.Histogram(x=df_PM10_errors_suburban['me'],nbinsx = 5,name='Mean Error (ME)'),
    'rural':go.Histogram(x=df_PM10_errors_rural['me'],nbinsx = 5,name='Mean Error (ME)'),
    'traffic':go.Histogram(x=df_PM10_errors_traffic['me'],nbinsx = 5,name='Mean Error (ME)'),
    'industrial':go.Histogram(x=df_PM10_errors_industrial['me'],nbinsx = 5,name='Mean Error (ME)')
}
NO2_histo_me={
    'all':go.Histogram(x=df_NO2_errors_all['me'], nbinsx = 10,name='Mean Error (ME)'),
    'urban':go.Histogram(x=df_NO2_errors_urban['me'],nbinsx = 10,name='Mean Error (ME)'),
    'suburban':go.Histogram(x=df_NO2_errors_suburban['me'],nbinsx = 5,name='Mean Error (ME)'),
    'rural':go.Histogram(x=df_NO2_errors_rural['me'],nbinsx = 5,name='Mean Error (ME)'),
    'traffic':go.Histogram(x=df_NO2_errors_traffic['me'],nbinsx = 5,name='Mean Error (ME)'),
    'industrial':go.Histogram(x=df_NO2_errors_industrial['me'],nbinsx = 5,name='Mean Error (ME)')
}
SO2_histo_me={
    'all':go.Histogram(x=df_SO2_errors_all['me'], nbinsx = 10,name='Mean Error (ME)'),
    'urban':go.Histogram(x=df_SO2_errors_urban['me'],nbinsx = 10,name='Mean Error (ME)'),
    'suburban':go.Histogram(x=df_SO2_errors_suburban['me'],nbinsx = 5,name='Mean Error (ME)'),
    'rural':go.Histogram(x=df_SO2_errors_rural['me'],nbinsx = 5,name='Mean Error (ME)'),
    'traffic':go.Histogram(x=df_SO2_errors_traffic['me'],nbinsx = 5,name='Mean Error (ME)'),
    'industrial':go.Histogram(x=df_SO2_errors_industrial['me'],nbinsx = 5,name='Mean Error (ME)')
}
O3_histo_me={
    'all':go.Histogram(x=df_O3_errors_all['me'], nbinsx = 10,name='Mean Error (ME)'),
    'urban':go.Histogram(x=df_O3_errors_urban['me'],nbinsx = 10,name='Mean Error (ME)'),
    'suburban':go.Histogram(x=df_O3_errors_suburban['me'],name='Mean Error (ME)'),
    'rural':go.Histogram(x=df_O3_errors_rural['me'],name='Mean Error (ME)'),
#    'traffic':go.Histogram(x=df_O3_errors_traffic['me'],nbinsx = 5), nie istnieje!
    'industrial':go.Histogram(x=df_O3_errors_industrial['me'],nbinsx = 5,name='Mean Error (ME)')
}
'''***********************************
Histograms - RMSD
*************************************'''
PM10_histo_rmsd={
    'all':go.Histogram(x=df_PM10_errors_all['rmsd'], nbinsx = 10,name='Root Mean Square Deviation (RMSD)'),
    'urban':go.Histogram(x=df_PM10_errors_urban['rmsd'],nbinsx = 10,name='Root Mean Square Deviation (RMSD)'),
    'suburban':go.Histogram(x=df_PM10_errors_suburban['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)'),
    'rural':go.Histogram(x=df_PM10_errors_rural['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)'),
    'traffic':go.Histogram(x=df_PM10_errors_traffic['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)'),
    'industrial':go.Histogram(x=df_PM10_errors_industrial['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)')
}
NO2_histo_rmsd={
    'all':go.Histogram(x=df_NO2_errors_all['rmsd'], nbinsx = 10,name='Root Mean Square Deviation (RMSD)'),
    'urban':go.Histogram(x=df_NO2_errors_urban['rmsd'],nbinsx = 10,name='Root Mean Square Deviation (RMSD)'),
    'suburban':go.Histogram(x=df_NO2_errors_suburban['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)'),
    'rural':go.Histogram(x=df_NO2_errors_rural['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)'),
    'traffic':go.Histogram(x=df_NO2_errors_traffic['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)'),
    'industrial':go.Histogram(x=df_NO2_errors_industrial['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)')
}
SO2_histo_rmsd={
    'all':go.Histogram(x=df_SO2_errors_all['rmsd'], nbinsx = 10,name='Root Mean Square Deviation (RMSD)'),
    'urban':go.Histogram(x=df_SO2_errors_urban['rmsd'],nbinsx = 10,name='Root Mean Square Deviation (RMSD)'),
    'suburban':go.Histogram(x=df_SO2_errors_suburban['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)'),
    'rural':go.Histogram(x=df_SO2_errors_rural['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)'),
    'traffic':go.Histogram(x=df_SO2_errors_traffic['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)'),
    'industrial':go.Histogram(x=df_SO2_errors_industrial['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)')
}
O3_histo_rmsd={
    'all':go.Histogram(x=df_O3_errors_all['rmsd'], nbinsx = 10,name='Root Mean Square Deviation (RMSD)'),
    'urban':go.Histogram(x=df_O3_errors_urban['rmsd'],nbinsx = 10,name='Root Mean Square Deviation (RMSD)'),
    'suburban':go.Histogram(x=df_O3_errors_suburban['rmsd'],name='Root Mean Square Deviation (RMSD)'),
    'rural':go.Histogram(x=df_O3_errors_rural['rmsd'],name='Root Mean Square Deviation (RMSD)'),
#    'traffic':go.Histogram(x=df_O3_errors_traffic['rmsd'],nbinsx = 5), nie istnieje!
    'industrial':go.Histogram(x=df_O3_errors_industrial['rmsd'],nbinsx = 5,name='Root Mean Square Deviation (RMSD)')
}
'''***********************************
Histograms - Corr
*************************************'''
PM10_histo_cor={
    'all':go.Histogram(x=df_PM10_errors_all['cor'], nbinsx = 10,name='Correlation coefficient'),
    'urban':go.Histogram(x=df_PM10_errors_urban['cor'],nbinsx = 10,name='Correlation coefficient'),
    'suburban':go.Histogram(x=df_PM10_errors_suburban['cor'],nbinsx = 5,name='Correlation coefficient'),
    'rural':go.Histogram(x=df_PM10_errors_rural['cor'],nbinsx = 5,name='Correlation coefficient'),
    'traffic':go.Histogram(x=df_PM10_errors_traffic['cor'],nbinsx = 5,name='Correlation coefficient'),
    'industrial':go.Histogram(x=df_PM10_errors_industrial['cor'],nbinsx = 5,name='Correlation coefficient')
}
NO2_histo_cor={
    'all':go.Histogram(x=df_NO2_errors_all['cor'], nbinsx = 10,name='Correlation coefficient'),
    'urban':go.Histogram(x=df_NO2_errors_urban['cor'],nbinsx = 10,name='Correlation coefficient'),
    'suburban':go.Histogram(x=df_NO2_errors_suburban['cor'],nbinsx = 5,name='Correlation coefficient'),
    'rural':go.Histogram(x=df_NO2_errors_rural['cor'],nbinsx = 5,name='Correlation coefficient'),
    'traffic':go.Histogram(x=df_NO2_errors_traffic['cor'],nbinsx = 5,name='Correlation coefficient'),
    'industrial':go.Histogram(x=df_NO2_errors_industrial['cor'],nbinsx = 5,name='Correlation coefficient')
}
SO2_histo_cor={
    'all':go.Histogram(x=df_SO2_errors_all['cor'], nbinsx = 10,name='Correlation coefficient'),
    'urban':go.Histogram(x=df_SO2_errors_urban['cor'],nbinsx = 10,name='Correlation coefficient'),
    'suburban':go.Histogram(x=df_SO2_errors_suburban['cor'],nbinsx = 5,name='Correlation coefficient'),
    'rural':go.Histogram(x=df_SO2_errors_rural['cor'],nbinsx = 5,name='Correlation coefficient'),
    'traffic':go.Histogram(x=df_SO2_errors_traffic['cor'],nbinsx = 5,name='Correlation coefficient'),
    'industrial':go.Histogram(x=df_SO2_errors_industrial['cor'],nbinsx = 5,name='Correlation coefficient')
}
O3_histo_cor={
    'all':go.Histogram(x=df_O3_errors_all['cor'], nbinsx = 10,name='Correlation coefficient'),
    'urban':go.Histogram(x=df_O3_errors_urban['cor'],nbinsx = 10,name='Correlation coefficient'),
    'suburban':go.Histogram(x=df_O3_errors_suburban['cor'],name='Correlation coefficient'),
    'rural':go.Histogram(x=df_O3_errors_rural['cor'],name='Correlation coefficient'),
#    'traffic':go.Histogram(x=df_O3_errors_traffic['cor'],nbinsx = 5), nie istnieje!
    'industrial':go.Histogram(x=df_O3_errors_industrial['cor'],nbinsx = 5,name='Correlation coefficient')
}
'''***********************************
Graphs objects - TS
*************************************'''



graph_PM10={
    'all':dcc.Graph(id='graphPM10',figure={'data':[PM10_gem_ts['all'],PM10_gios_ts['all'],
        PM10_gem_p25['all'],PM10_gem_p75['all'],
        PM10_gios_p25['all'],PM10_gios_p75['all']],
        'layout': layout_ts_PM10}),

    'urban':dcc.Graph(id='graphPM10',figure={'data':[PM10_gem_ts['urban'],PM10_gios_ts['urban'],
        PM10_gem_p25['urban'],PM10_gem_p75['urban'],
        PM10_gios_p25['urban'],PM10_gios_p75['urban']],
        'layout': layout_ts_PM10}),

    'suburban':dcc.Graph(id='graphPM10',figure={'data':[PM10_gem_ts['suburban'],PM10_gios_ts['suburban'],
        PM10_gem_p25['suburban'],PM10_gem_p75['suburban'],
        PM10_gios_p25['suburban'],PM10_gios_p75['suburban']],
        'layout': layout_ts_PM10}),

    'rural':dcc.Graph(id='graphPM10',figure={'data':[PM10_gem_ts['rural'],PM10_gios_ts['rural'],
        PM10_gem_p25['rural'],PM10_gem_p75['rural'],
        PM10_gios_p25['rural'],PM10_gios_p75['rural']],
        'layout': layout_ts_PM10}),

    'industrial':dcc.Graph(id='graphPM10',figure={'data':[PM10_gem_ts['industrial'],PM10_gios_ts['industrial'],
        PM10_gem_p25['industrial'],PM10_gem_p75['industrial'],
        PM10_gios_p25['industrial'],PM10_gios_p75['industrial']],
        'layout': layout_ts_PM10}),

    'traffic':dcc.Graph(id='graphPM10',figure={'data':[PM10_gem_ts['traffic'],PM10_gios_ts['traffic'],
        PM10_gem_p25['traffic'],PM10_gem_p75['traffic'],
        PM10_gios_p25['traffic'],PM10_gios_p75['traffic']],
        'layout': layout_ts_PM10})
}
graph_NO2={
    'all':dcc.Graph(id='graphNO2',figure={'data':[NO2_gem_ts['all'],NO2_gios_ts['all'],
        NO2_gem_p25['all'],NO2_gem_p75['all'],
        NO2_gios_p25['all'],NO2_gios_p75['all']],
        'layout': layout_ts_NO2}),

    'urban':dcc.Graph(id='graphNO2',figure={'data':[NO2_gem_ts['urban'],NO2_gios_ts['urban'],
        NO2_gem_p25['urban'],NO2_gem_p75['urban'],
        NO2_gios_p25['urban'],NO2_gios_p75['urban']],
        'layout': layout_ts_NO2}),

    'suburban':dcc.Graph(id='graphNO2',figure={'data':[NO2_gem_ts['suburban'],NO2_gios_ts['suburban'],
        NO2_gem_p25['suburban'],NO2_gem_p75['suburban'],
        NO2_gios_p25['suburban'],NO2_gios_p75['suburban']],
        'layout': layout_ts_NO2}),

    'rural':dcc.Graph(id='graphNO2',figure={'data':[NO2_gem_ts['rural'],NO2_gios_ts['rural'],
        NO2_gem_p25['rural'],NO2_gem_p75['rural'],
        NO2_gios_p25['rural'],NO2_gios_p75['rural']],
        'layout': layout_ts_NO2}),

    'industrial':dcc.Graph(id='graphNO2',figure={'data':[NO2_gem_ts['industrial'],NO2_gios_ts['industrial'],
        NO2_gem_p25['industrial'],NO2_gem_p75['industrial'],
        NO2_gios_p25['industrial'],NO2_gios_p75['industrial']],
        'layout': layout_ts_NO2}),

    'traffic':dcc.Graph(id='graphNO2',figure={'data':[NO2_gem_ts['traffic'],NO2_gios_ts['traffic'],
        NO2_gem_p25['traffic'],NO2_gem_p75['traffic'],
        NO2_gios_p25['traffic'],NO2_gios_p75['traffic']],
        'layout': layout_ts_NO2})
}

graph_SO2={
    'all':dcc.Graph(id='graphSO2',figure={'data':[SO2_gem_ts['all'],SO2_gios_ts['all'],
        SO2_gem_p25['all'],SO2_gem_p75['all'],
        SO2_gios_p25['all'],SO2_gios_p75['all']],
        'layout': layout_ts_SO2}),

    'urban':dcc.Graph(id='graphSO2',figure={'data':[SO2_gem_ts['urban'],SO2_gios_ts['urban'],
        SO2_gem_p25['urban'],SO2_gem_p75['urban'],
        SO2_gios_p25['urban'],SO2_gios_p75['urban']],
        'layout': layout_ts_SO2}),

    'suburban':dcc.Graph(id='graphSO2',figure={'data':[SO2_gem_ts['suburban'],SO2_gios_ts['suburban'],
        SO2_gem_p25['suburban'],SO2_gem_p75['suburban'],
        SO2_gios_p25['suburban'],SO2_gios_p75['suburban']],
        'layout': layout_ts_SO2}),

    'rural':dcc.Graph(id='graphSO2',figure={'data':[SO2_gem_ts['rural'],SO2_gios_ts['rural'],
        SO2_gem_p25['rural'],SO2_gem_p75['rural'],
        SO2_gios_p25['rural'],SO2_gios_p75['rural']],
        'layout': layout_ts_SO2}),

    'industrial':dcc.Graph(id='graphSO2',figure={'data':[SO2_gem_ts['industrial'],SO2_gios_ts['industrial'],
        SO2_gem_p25['industrial'],SO2_gem_p75['industrial'],
        SO2_gios_p25['industrial'],SO2_gios_p75['industrial']],
        'layout': layout_ts_SO2}),

    'traffic':dcc.Graph(id='graphSO2',figure={'data':[SO2_gem_ts['traffic'],SO2_gios_ts['traffic'],
        SO2_gem_p25['traffic'],SO2_gem_p75['traffic'],
        SO2_gios_p25['traffic'],SO2_gios_p75['traffic']],
        'layout': layout_ts_SO2})
}

graph_O3={
    'all':dcc.Graph(id='graphO3',figure={'data':[O3_gem_ts['all'],O3_gios_ts['all'],
        O3_gem_p25['all'],O3_gem_p75['all'],
        O3_gios_p25['all'],O3_gios_p75['all']],
        'layout': layout_ts_O3}),

    'urban':dcc.Graph(id='graphO3',figure={'data':[O3_gem_ts['urban'],O3_gios_ts['urban'],
        O3_gem_p25['urban'],O3_gem_p75['urban'],
        O3_gios_p25['urban'],O3_gios_p75['urban']],
        'layout': layout_ts_O3}),

    'suburban':dcc.Graph(id='graphO3',figure={'data':[O3_gem_ts['suburban'],O3_gios_ts['suburban'],
        O3_gem_p25['suburban'],O3_gem_p75['suburban'],
        O3_gios_p25['suburban'],O3_gios_p75['suburban']],
        'layout': layout_ts_O3}),

    'rural':dcc.Graph(id='graphO3',figure={'data':[O3_gem_ts['rural'],O3_gios_ts['rural'],
        O3_gem_p25['rural'],O3_gem_p75['rural'],
        O3_gios_p25['rural'],O3_gios_p75['rural']],
        'layout': layout_ts_O3}),

    'industrial':dcc.Graph(id='graphO3',figure={'data':[O3_gem_ts['industrial'],O3_gios_ts['industrial'],
        O3_gem_p25['industrial'],O3_gem_p75['industrial'],
        O3_gios_p25['industrial'],O3_gios_p75['industrial']],
        'layout': layout_ts_O3})
}
'''***********************************
Graphs objects - SCATTER PLOTS
*************************************'''
'''
graph_PM10_scatter={
    'all':dcc.Graph(id='graphPM10',figure={'data':[PM10_scatter['all']],
        'layout': generateScatterLayout('PM10')}),

    'urban':dcc.Graph(id='graphPM10',figure={'data':[PM10_scatter['urban']],
        'layout': generateScatterLayout('PM10')}),

    'suburban':dcc.Graph(id='graphPM10',figure={'data':[PM10_scatter['suburban']],
        'layout': generateScatterLayout('PM10')}),

    'rural':dcc.Graph(id='graphPM10',figure={'data':[PM10_scatter['rural']],
        'layout': generateScatterLayout('PM10')}),

    'industrial':dcc.Graph(id='graphPM10',figure={'data':[PM10_scatter['industrial']],
        'layout': generateScatterLayout('PM10')}),

    'traffic':dcc.Graph(id='graphPM10',figure={'data':[PM10_scatter['traffic']],
        'layout': generateScatterLayout('PM10')})
}
graph_NO2_scatter={
    'all':dcc.Graph(id='graphNO2',figure={'data':[NO2_scatter['all']],
        'layout': generateScatterLayout('NO2')}),

    'urban':dcc.Graph(id='graphNO2',figure={'data':[NO2_scatter['urban']],
        'layout': generateScatterLayout('NO2')}),

    'suburban':dcc.Graph(id='graphNO2',figure={'data':[NO2_scatter['suburban']],
        'layout': generateScatterLayout('NO2')}),

    'rural':dcc.Graph(id='graphNO2',figure={'data':[NO2_scatter['rural']],
        'layout': generateScatterLayout('NO2')}),

    'industrial':dcc.Graph(id='graphNO2',figure={'data':[NO2_scatter['industrial']],
        'layout': generateScatterLayout('NO2')}),

    'traffic':dcc.Graph(id='graphNO2',figure={'data':[NO2_scatter['traffic']],
        'layout': generateScatterLayout('NO2')})
}
graph_SO2_scatter={
    'all':dcc.Graph(id='graphSO2',figure={'data':[SO2_scatter['all']],
        'layout': generateScatterLayout('SO2')}),

    'urban':dcc.Graph(id='graphSO2',figure={'data':[SO2_scatter['urban']],
        'layout': generateScatterLayout('SO2')}),

    'suburban':dcc.Graph(id='graphSO2',figure={'data':[SO2_scatter['suburban']],
        'layout': generateScatterLayout('SO2')}),

    'rural':dcc.Graph(id='graphSO2',figure={'data':[SO2_scatter['rural']],
        'layout': generateScatterLayout('SO2')}),

    'industrial':dcc.Graph(id='graphSO2',figure={'data':[SO2_scatter['industrial']],
        'layout': generateScatterLayout('SO2')}),

    'traffic':dcc.Graph(id='graphSO2',figure={'data':[SO2_scatter['traffic']],
        'layout': generateScatterLayout('SO2')})
}
graph_O3_scatter={
    'all':dcc.Graph(id='graphO3',figure={'data':[O3_scatter['all']],
        'layout': generateScatterLayout('O3')}),

    'urban':dcc.Graph(id='graphO3',figure={'data':[O3_scatter['urban']],
        'layout': generateScatterLayout('O3')}),

    'suburban':dcc.Graph(id='graphO3',figure={'data':[O3_scatter['suburban']],
        'layout': generateScatterLayout('O3')}),

    'rural':dcc.Graph(id='graphO3',figure={'data':[O3_scatter['rural']],
        'layout': generateScatterLayout('O3')}),

    'industrial':dcc.Graph(id='graphO3',figure={'data':[O3_scatter['industrial']],
        'layout': generateScatterLayout('O3')}),


}'''

graph_histo_mae={
    'all':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_mae['all'],NO2_histo_mae['all'],SO2_histo_mae['all'],O3_histo_mae['all']],'layout':layout_histo}),
    'urban':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_mae['urban'],NO2_histo_mae['urban'],SO2_histo_mae['urban'],O3_histo_mae['urban']],'layout':layout_histo}),
    'suburban':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_mae['suburban'],NO2_histo_mae['suburban'],SO2_histo_mae['suburban'],O3_histo_mae['suburban']],'layout':layout_histo}),
    'traffic':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_mae['traffic'],NO2_histo_mae['suburban'],SO2_histo_mae['suburban'],O3_histo_mae['suburban']],'layout':layout_histo})
}
graph_histo_me={
    'all':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_me['all'],NO2_histo_me['all'],SO2_histo_me['all'],O3_histo_me['all']],'layout':layout_histo}),
    'urban':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_me['urban'],NO2_histo_me['urban'],SO2_histo_me['urban'],O3_histo_me['urban']],'layout':layout_histo}),
    'suburban':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_me['suburban'],NO2_histo_me['suburban'],SO2_histo_me['suburban'],O3_histo_me['suburban']],'layout':layout_histo}),
    'traffic':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_me['traffic'],NO2_histo_me['suburban'],SO2_histo_me['suburban'],O3_histo_me['suburban']],'layout':layout_histo})
}
graph_histo_rmsd={
    'all':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_rmsd['all'],NO2_histo_rmsd['all'],SO2_histo_rmsd['all'],O3_histo_rmsd['all']],'layout':layout_histo}),
    'urban':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_rmsd['urban'],NO2_histo_rmsd['urban'],SO2_histo_rmsd['urban'],O3_histo_rmsd['urban']],'layout':layout_histo}),
    'suburban':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_rmsd['suburban'],NO2_histo_rmsd['suburban'],SO2_histo_rmsd['suburban'],O3_histo_rmsd['suburban']],'layout':layout_histo}),
    'traffic':dcc.Graph(id='graphPM10',figure={'data':[PM10_histo_rmsd['traffic'],NO2_histo_rmsd['suburban'],SO2_histo_rmsd['suburban'],O3_histo_rmsd['suburban']],'layout':layout_histo})
}
