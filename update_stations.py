#!/usr/bin/python3
import GIOS_API
import time
import pprint
from db_interface import DBConnection
import urllib.request, json 
import urllib.error as error
'''
skrypt do aktualizacji listy stacji
'''

def main():
	try:
		print("Downloading stations...")
		downloadAndStoreStations()
	except Exception as e:
		print("Error downloading stations")
		print(e)

	try:
		print("Downloading devices...")
		downloadAndStoreDevices()
	except Exception as e:
		print("Error downloading devices")
		print(e)
		
		
def downloadAndStoreDevices():
	'''for all the stations, download and store devices'''
	db=DBConnection()
	db.connect()

	stations=db.getStations()
	for st in stations:
		try:
			data=GIOS_API.download_sensors_list(str(st[0]))
			db.storeMeasurements(data)
			time.sleep(1)
			print('Saved '+str(st[1]))
		except error.HTTPError as e:
			print('Error downloading '+str(st[1]))
			print(e)
	db=None

def downloadAndStoreStations():
	'''download and store list of GIOS stations'''
	stacje = GIOS_API.download_station_list()
	db=DBConnection()
	db.connect()
	db.storeStations(stacje)
	db=None

if __name__ =="__main__":
	main()