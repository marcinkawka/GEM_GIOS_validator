import urllib.request, json 
import pandas as pd
import sqlite3
import pprint
import numpy as np
import time
from urllib.error import URLError, HTTPError
from sqlite3 import IntegrityError,OperationalError
#
# Quick and dirty prototype for downloading GIOS data and storing them in SQLite db
#
#	TODO: Separate download from storing abstraction
#
pp = pprint.PrettyPrinter(indent=4)

stations_url="http://api.gios.gov.pl/pjp-api/rest/station/findAll"
dev_url="http://api.gios.gov.pl/pjp-api/rest/station/sensors/" #+stationID 
data_url="http://api.gios.gov.pl/pjp-api/rest/data/getData/" #+devID
index_url="http://api.gios.gov.pl/pjp-api/rest/aqindex/getIndex/"#+stationID

def download_station_list(conn):
	with urllib.request.urlopen(stations_url) as url:
		data = json.loads(url.read().decode())
		#pp.pprint(data)

	df=pd.DataFrame(data)
	df2=df[['stationName','addressStreet','id','gegrLat','gegrLon']]
	df2.to_sql(name='stations',con=conn,if_exists='fail')
	conn.commit()


def download_sensors_list(conn):
	df=pd.read_sql(con=conn,sql="select * from stations")
	#print(df)
	for sid in df['id']:
		s_url=dev_url+str(sid)
		
		with urllib.request.urlopen(s_url) as url:
			data = json.loads(url.read().decode())
		
		pp.pprint(data)
		for d in data:
			id_measurement =d['id']
			id_station=d['stationId']
			paramName=d['param']['paramCode']
			try:
				sql="INSERT INTO measurements(id_measurement,id_station,paramName) \
					VALUES("+str(id_measurement)+","+str(id_station)+",\""+paramName+"\")		"
				cur=conn.cursor()
				cur.execute(sql)
				conn.commit()
				print("Added "+str(id_station))
			except Exception as e:
				print("Error adding "+str(e))
			
def list_devices(conn):
#populates devices list from SQLite db
	sql="select id_measurement from measurements"
	cur=conn.cursor()
	cur.execute(sql)
	rows=cur.fetchall()
	for devID in rows:
		download_measurements(devID[0],conn)
		time.sleep(2)


def download_measurements(devID,conn):			
# downloads measurements of selected deviceID
	d_url=data_url+str(devID)
	
	try:
		with urllib.request.urlopen(d_url) as url:
			data = json.loads(url.read().decode())
		
			try:
				df=pd.DataFrame(data['values'])	


				df['date2']=pd.DatetimeIndex(pd.to_datetime(df['date']))
				del df['date']
				df.set_index('date2',inplace=True) 
				#timestamp in ms
				df['unix_date']= (df.index.astype(np.int64)/1000000).astype(np.int64)
				df['id_measurement']=devID
				df.set_index('unix_date',inplace=True)
				
				records= df.to_records()
				query='''insert  into results(unix_date,value,id_measurement) values ({0},{1},{2}) '''
				c = conn.cursor()
				for rec in records:
					query2=query.format(rec[0],rec[1],rec[2])
					query2=query2.replace("nan","NULL")
					
					try:
						c.execute(query2)
						conn.commit()
				
				
					except IntegrityError:
						pass	#in case of duplicate

					except OperationalError:
						print("Empty value")
						print(query2)
						pass	

					except KeyError:
						print("Error processing query "+str(devID))
						print(query2)	
						pass 
			except Exception as e:
				print("Error processing "+str(devID))
				print(e)
	except HTTPError as e:
		print('Error connecting to GIOS')
			
	
def main():
	baza='/home/mkawka/pythony/dash_trials/GIOS_db.sqlite'
	conn = sqlite3.connect(baza)
	#done once, enough for now
	#download_station_list(conn)
	#
	#download_sensors_list(conn)
	list_devices(conn)
	conn.close()

if __name__ == "__main__":
	main()	