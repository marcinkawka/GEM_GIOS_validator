import pprint
from db_interface import DBConnection
from psycopg2 import sql
import psycopg2
import pandas as pd



def updateCodes():
	db=DBConnection()
	db.connect()
	stations =db.getStations()

	df=pd.read_csv('/home/mkawka/pythony/lista_stacji_WIOS/kody_stacji.csv')
	df2=df[['id_station','station_code']]
	for st in stations:
		code=(df2[df2['id_station']==st[0]]['station_code']).to_string(index=False)
		print(str(st[0])+'\t'+str(code))
		db.updateStationCode(staion_id=st[0],station_code=str(code))

def updateTypes():
	db=DBConnection()
	db.connect()
	stations =db.getStations()

	df=pd.read_csv('/home/mkawka/pythony/lista_stacji_WIOS/stacje2018.csv')
	df2=df[['station_code','Typ stacji','Typ obszaru']]
	for st in stations:
		
		code=str(st[5])
		id=str(st[0])
		if code!='None':
			st_type=str(df2[df2['station_code']==code]['Typ stacji'].to_string(index=False))
			area_type=str(df2[df2['station_code']==code]['Typ obszaru'].to_string(index=False))

			print(code+' '+st_type+' '+area_type)
			db.updateStationType(staion_id=st[0],station_type=st_type)
			db.updateAreaType(staion_id=st[0],area_type=area_type)

def main():
	updateTypes()

	
if __name__ =="__main__":
	main()